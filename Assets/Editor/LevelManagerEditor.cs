using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AI;
using UnityEngine.AI;

[CustomEditor(typeof(LevelManager)), CanEditMultipleObjects]
public class LevelManagerEditor : Editor
{
    private void OnEnable()
    {
        EditorUtility.SetDirty((LevelManager)target);
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Level Manager Editor", EditorStyles.boldLabel);
        LevelManager levelManager = (LevelManager)target;
        if (GUILayout.Button("Setup Level"))
        {
            levelManager.GeneratePointList();
            BakeNavMesh(levelManager);
        }
    }
    public void BakeNavMesh(LevelManager levelManager)
    {
        NavMeshSurface navMeshSurface = levelManager.GetComponentInChildren<NavMeshSurface>();
        navMeshSurface.name = string.Format("Ground-{0}", levelManager.gameObject.name);
        Object[] objects = new Object[] { navMeshSurface };
        NavMeshAssetManager.instance.ClearSurfaces(objects);
        SceneView.RepaintAll();
        NavMeshAssetManager.instance.StartBakingSurfaces(objects);
    }
}
