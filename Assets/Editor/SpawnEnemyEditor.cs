using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(SpawnEnemy)), CanEditMultipleObjects]
public class SpawnEnemyEditor : Editor
{
    private int m_EnemyIndex;
    private void OnEnable()
    {
        EditorUtility.SetDirty((SpawnEnemy)target);
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Spawn Enemy Editor", EditorStyles.boldLabel);

        SpawnEnemy spawnEnemy = (SpawnEnemy)target;
        // Choose Enemy
        List<GameObject> enemyList = PrefabManager.Instance.m_EnemyList;
        List<string> enemyNameList = new List<string>();
        foreach (GameObject enemy in enemyList)
        {
            enemyNameList.Add(enemy.name);
        }
        m_EnemyIndex = EditorGUILayout.Popup("Enemy", m_EnemyIndex, enemyNameList.ToArray());
        spawnEnemy.enemyPrefab = enemyList[m_EnemyIndex];

        // Choose Type Spawn
        spawnEnemy.spawnType = (SpawnType)EditorGUILayout.EnumPopup("Spawn Type", spawnEnemy.spawnType);

        // Choos Spawn Count
        switch (spawnEnemy.spawnType)
        {
            case SpawnType.Random:
                spawnEnemy.m_EnemyMinSpawn = EditorGUILayout.IntField("Enemy Min Spawn", spawnEnemy.m_EnemyMinSpawn);
                spawnEnemy.m_EnemyMaxSpawn = EditorGUILayout.IntField("Enemy Max Spawn", spawnEnemy.m_EnemyMaxSpawn);
                break;
            case SpawnType.NotRandom:
                spawnEnemy.m_EnemySpawn = EditorGUILayout.IntField("Enemy Count", spawnEnemy.m_EnemySpawn);
                break;
        }
    }
}
