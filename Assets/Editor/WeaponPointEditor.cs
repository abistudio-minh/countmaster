using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
[CustomEditor(typeof(WeaponPoint)), CanEditMultipleObjects]
public class WeaponPointEditor : Editor
{
    // TODO: +++++ Fix All Editor Later
    private int index;
    private void OnEnable()
    {
        EditorUtility.SetDirty((WeaponPoint)target);
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Weapon Point Editor", EditorStyles.boldLabel);

        WeaponPoint weaponPoint = (WeaponPoint)target;

        // Choose Weapon
        List<WeaponData> weaponDataList = WeaponManager.Instance.m_WeaponList;
        List<string> weaponList = new List<string>();
        foreach (WeaponData weaponData in weaponDataList)
        {
            weaponList.Add(weaponData.weaponName);
        }


        index = EditorGUILayout.Popup("Weapon", index, weaponList.ToArray());
        weaponPoint.m_WeaponData = weaponDataList[index];
    }
}
