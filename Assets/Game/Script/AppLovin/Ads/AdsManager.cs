﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public enum AdsMediationType {
    ADMOB
}
public class AdsConfig {
    public double maxAdsTime = 120f;
    public double minAdsTime = 30f;
    public int remainingSkipAds = 1;
    public AdsMediationType adsMediationType = AdsMediationType.ADMOB;
    public void DiceMediationType() {

    }
}
public class AdsManager : Singleton<AdsManager> 
{

    private double m_AdsLoadingCooldown = 0f;
    private double m_MaxLoadingCooldown = 5f;

    private double m_AdsTime = 0;
    private int m_CountingShownAds = 0;
    private int m_MaxShowOffer = 6;
    private int m_NumberShowOffer = 6;
    private int m_RemainingSkipInterstital = 1;
    private int m_SkipTime = 0;
    private string m_AdsConfigString = "iviviio";

    private string m_DefaultAdsString = "iviviio";
    private AdsConfig m_RewardVideoAdsConfig;
    private AdsConfig m_InterstitialAdsConfig;
    private AdsConfig m_BannerAdsConfig;
    private UnityAction m_InterstitialAdsClosedCallback;
    //public AdmobMediationController m_AdmobMediationController;
    public AdsMediationController m_AdsMediationController;
#if UNITY_IRONSOURCE
    public IronSourceMediationController m_AdsMediationController;
#endif

    private void Awake() {
        DontDestroyOnLoad(gameObject);
        RewardAdsManager.Instance.Init();
        AdsMediationType choosedType = AdsMediationType.ADMOB;
        Debug.Log("Use mediation platform =============== " + choosedType);
        choosedType = AdsMediationType.ADMOB;
        m_InterstitialAdsConfig = new AdsConfig();
#if UNITY_ANDROID
        m_InterstitialAdsConfig.adsMediationType = choosedType;
#elif UNITY_IOS
        m_InterstitialAdsConfig.adsMediationType = AdsMediationType.ADMOB;
#endif
        m_RewardVideoAdsConfig = new AdsConfig();
        m_RewardVideoAdsConfig.adsMediationType = choosedType;

        m_BannerAdsConfig = new AdsConfig();
        m_BannerAdsConfig.adsMediationType = AdsMediationType.ADMOB;
        UpdateAdsMediation();
    }
    private void Start() {
    }
    private void Update() {
        if (m_AdsTime > 0) {
            m_AdsTime -= Time.deltaTime;
        }
        if (m_AdsLoadingCooldown > 0) {
            m_AdsLoadingCooldown -= Time.deltaTime;
            if (m_AdsLoadingCooldown <= 0) {
                if (!IsRewardVideoLoaded()) {
                    RequestRewardBasedVideo();
                }
                if (!IsInterstitialAdLoaded()) {
                    RequestInterstitial();
                }
            }
        }
    }
    public void UpdateAdsMediation() {
        //Debug.Log("Update Ads Mediation Rewards " + m_RewardVideoAdsConfig.adsMediationType + " Interstitial " + m_InterstitialAdsConfig.adsMediationType);
        if (m_RewardVideoAdsConfig.adsMediationType == AdsMediationType.ADMOB) {
            m_AdsMediationController.IsActive = true;
            m_AdsMediationController.gameObject.SetActive(true);
            //Debug.Log("admob video " + m_AdmobMediationController.IsActive);
        }

        if (m_InterstitialAdsConfig.adsMediationType == AdsMediationType.ADMOB) {
            m_AdsMediationController.IsActive = true;
            m_AdsMediationController.gameObject.SetActive(true);
            //Debug.Log("Admob Inters " + m_AdmobMediationController.IsActive);
        }
        InitAdsMediation();

        //Setup Interstitial
        SetupInterstitial();

        //Setup Reward Video
        SetupRewardVideo();

        //Setup Banner
        SetupBannerAds();
    }
    public void InitAdsMediation() {
        if (!GetSelectedInterstitialMediation().IsInited) {
            //Debug.Log("Init " + GetSelectedInterstitialMediation().name + " " + GetSelectedInterstitialMediation().IsActive);
            GetSelectedInterstitialMediation().Init();
        }
        if (!GetSelectedRewardVideosMediation().IsInited) {
            //Debug.Log("Init " + GetSelectedRewardVideosMediation().name + " " + GetSelectedRewardVideosMediation().IsActive);
            GetSelectedRewardVideosMediation().Init();
        }
    }
    public AdsMediationController GetSelectedInterstitialMediation() {
        switch (m_InterstitialAdsConfig.adsMediationType) {
            case AdsMediationType.ADMOB: {
                    m_AdsMediationController.IsActive = true;
                    return m_AdsMediationController;
                }
        }
        return null;
    }
    public AdsMediationController GetSelectedRewardVideosMediation() {
        switch (m_RewardVideoAdsConfig.adsMediationType) {
            case AdsMediationType.ADMOB: {
                    m_AdsMediationController.IsActive = true;
                    return m_AdsMediationController;
                }
        }
        return null;
    }
    public AdsMediationController GetSelectedBannerAdsMediation() {
        switch (m_BannerAdsConfig.adsMediationType) {
            case AdsMediationType.ADMOB: {
                    m_AdsMediationController.IsActive = true;
                    return m_AdsMediationController;
                }
        }
        return null;
    }
    private void SetupInterstitial() {
        GetSelectedInterstitialMediation().InitInterstitialAd(OnInterstitialAdClosed, OnInterstitialAdFailedToLoad, OnInterstitialAdSuccessToLoad);
    }

    public void RequestInterstitial() {
        if (GetSelectedInterstitialMediation().IsInterstitialLoaded()) return;
#if !UNITY_EDITOR
        GetSelectedInterstitialMediation().RequestInterstitialAd();
#endif
        //Debug.Log("INTERSTITIAL " + GetSelectedInterstitialMediation().ToString());
    }
    public void ShowInterstitial(UnityAction callback, UnityAction showSuccessCallback, bool isIngame = true)
    {

        m_InterstitialAdsClosedCallback = callback;
        ShowSelectedInterstitialAd(showSuccessCallback);
#if UNITY_EDITOR
        OnInterstitialAdClosed();
#endif
    }

    private void UpdateMaxCooldownRewardVideo() {
    }
    private bool IsGoodToShowOffer() {
        if (m_NumberShowOffer == 0) {
            return true;
        } else {
            if (m_NumberShowOffer > 0) {
                ConsumeNumberShowOffer();
            }
            return false;
        }
    }

    private void ShowSelectedInterstitialAd(UnityAction showSuccessCallback) {
        GetSelectedInterstitialMediation().ShowInterstitialAd(showSuccessCallback);
    }
    public bool IsInterstitialAdLoaded() {
        bool isInterstitialAdLoaded = GetSelectedInterstitialMediation().IsInterstitialLoaded();
        return isInterstitialAdLoaded;
    }
    public void ResetAdsLoadingCooldown() {
        m_AdsLoadingCooldown = m_MaxLoadingCooldown;
    }
    private void OnInterstitialAdFailedToLoad() {
        ResetAdsLoadingCooldown();
    }
    public void OnInterstitialAdSuccessToLoad() {
    }
    private void OnInterstitialAdClosed() {
        RequestInterstitial();
        if (m_InterstitialAdsClosedCallback != null) {
            EventManager.AddEventNextFrame(m_InterstitialAdsClosedCallback);
        }

    }

#region Banner Ads
    private void SetupBannerAds() {
        GetSelectedBannerAdsMediation().InitBannerAds();
    }
    public void ShowBannerAds() {
        GetSelectedBannerAdsMediation().ShowBannerAds();
    }
    public void HideBannerAds() {
        GetSelectedBannerAdsMediation().HideBannerAds();
    }
#endregion

#region Reward Ads
    // Reward Video Setup
    private void SetupRewardVideo() {
        GetSelectedRewardVideosMediation().InitRewardVideoAd(HandleRewardBasedVideoClosed, HandleRewardBasedVideoFailedToLoad);
    }
    public void RequestRewardBasedVideo() {
        if (GetSelectedRewardVideosMediation().IsRewardVideoLoaded()) return;
        GetSelectedRewardVideosMediation().RequestRewardVideoAd();
    }
    public void ShowRewardVideo(UnityAction successCallback, UnityAction failedCallback = null) {


        if (IsReadyToShowRewardVideo()) {
            GetSelectedRewardVideosMediation().ShowRewardVideoAd(successCallback, failedCallback);
        }
    }
    private void ShowSelectedRewardVideo(UnityAction successCallback, UnityAction failedCallback = null) {
        GetSelectedRewardVideosMediation().ShowRewardVideoAd(successCallback, failedCallback);
    }
    public bool IsRewardVideoLoaded() {
        return GetSelectedRewardVideosMediation().IsRewardVideoLoaded();
    }
    private void HandleRewardBasedVideoClosed() {
        ResetAdsLoadingCooldown();
        RequestRewardBasedVideo();
    }
    private void HandleRewardBasedVideoFailedToLoad() {
        ResetAdsLoadingCooldown();
    }
#endregion Reward Ads

    public void ConsumeNumberShowInterstitial(int amount = 1) {
        m_RemainingSkipInterstital -= amount;
    }
    public void ConsumeNumberShowOffer(int amount = 1) {
        m_NumberShowOffer -= amount;
    }
    public void SetSkipTime(int amount) {
        m_SkipTime = amount;
    }
    public bool IsReadyToShowInterstitial() {
        if (IsInterstitialAdLoaded()) {
            return true;
        }
        return false;
    }
    public bool IsReadyToShowRewardVideo() {
        if (IsRewardVideoLoaded()) {
            return true;
        }
        return false;
    }
}