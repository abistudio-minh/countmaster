﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AdsMediationController : MonoBehaviour 
{
    public bool IsActive = false;
    protected UnityAction m_InterstitialAdCloseCallback;
    protected UnityAction m_InterstitialAdLoadFailCallback;
    protected UnityAction m_InterstitialAdShowSuccessCallback;
    protected UnityAction m_WatchVideoSuccessCallback;
    protected UnityAction m_WatchVideoFailCallback;
    protected UnityAction m_CloseRewardVideoCallback;
    protected UnityAction m_LoadRewardVideoFailedCallback;
    public bool IsInited = false;
    public virtual void Init()
    {
        IsInited = true;
    }
    public virtual void InitBannerAds()
    {
    }
    public virtual void RequestBannerAds()
    {
    }
    public virtual void ShowBannerAds()
    {
    }
    public virtual void HideBannerAds()
    {
    }
    public virtual void InitInterstitialAd(UnityAction adClosedCallback, UnityAction adLoadFailedCallback, UnityAction adLoadSuccessCallback)
    {
        m_InterstitialAdCloseCallback = adClosedCallback;
        m_InterstitialAdLoadFailCallback = adLoadFailedCallback;
        m_InterstitialAdShowSuccessCallback = adLoadSuccessCallback;
    }
    public virtual void ShowInterstitialAd(UnityAction intersShowSuccessCallback)
    {
    }
    public virtual void RequestInterstitialAd()
    {
    }
    public virtual bool IsInterstitialLoaded()
    {
        return false;
    }
    public virtual void InitRewardVideoAd(UnityAction videoClosed, UnityAction videoLoadFailed)
    {
        m_CloseRewardVideoCallback = videoClosed;
        m_LoadRewardVideoFailedCallback = videoLoadFailed;
    }
    public virtual void RequestRewardVideoAd()
    {
    }
    public virtual void ShowRewardVideoAd(UnityAction successCallback, UnityAction failedCallback)
    {
        m_WatchVideoSuccessCallback = successCallback;
        m_WatchVideoFailCallback = failedCallback;
    }
    public virtual bool IsRewardVideoLoaded()
    {
        return false;
    }
}
