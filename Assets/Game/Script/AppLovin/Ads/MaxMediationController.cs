using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MaxMediationController : AdsMediationController {

    public string m_MaxSdkKey = "";

    private string m_ApplicationKey;
    private bool m_IsWatchSuccess = false;

    public string m_RewardAdUnitID;
    public string m_InterstitialAdUnitID;
    public string m_BannerAdUnitID;

    private void Awake()
    {
    }
    private void Start()
    {

#if UNITY_ANDROID
        m_ApplicationKey = m_MaxSdkKey;
#endif
        Debug.Log("unity-script: MyAppStart Start called");

        MaxSdkCallbacks.OnSdkInitializedEvent += sdkConfiguration => {
            // AppLovin SDK is initialized, configure and start loading ads.
            Debug.Log("MAX SDK Initialized");
            AdsManager.Instance.UpdateAdsMediation();
        };
        MaxSdk.SetSdkKey(m_MaxSdkKey);
        MaxSdk.InitializeSdk();
        Debug.Log("Ironsource Finish Start");
        //ShowBannerAds();
    }

    #region Interstitial
    public override void InitInterstitialAd(UnityAction adClosedCallback, UnityAction adLoadFailedCallback, UnityAction adLoadSuccessCallback)
    {
        base.InitInterstitialAd(adClosedCallback, adLoadFailedCallback, adLoadSuccessCallback);
        Debug.Log("Init ironsource Interstitial");

        // Attach callbacks
        MaxSdkCallbacks.Interstitial.OnAdLoadedEvent += OnInterstitialLoadedEvent;
        MaxSdkCallbacks.Interstitial.OnAdLoadFailedEvent += OnInterstitialFailedEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayFailedEvent += InterstitialFailedToDisplayEvent;
        MaxSdkCallbacks.Interstitial.OnAdHiddenEvent += OnInterstitialDismissedEvent;
        //MaxSdkCallbacks.Interstitial.OnAdRevenuePaidEvent += OnInterstitialRevenuePaidEvent;
        MaxSdkCallbacks.Interstitial.OnAdDisplayedEvent += OnInterstitialAdShowSucceededEvent;


        // Load the first interstitial
        RequestInterstitialAd();
    }
    public override void RequestInterstitialAd()
    {
        base.RequestInterstitialAd();
        Debug.Log("Request ironsource Interstitial");
        MaxSdk.LoadInterstitial(m_InterstitialAdUnitID);
    }
    public override void ShowInterstitialAd(UnityAction intersShowSuccessCallback)
    {
        base.ShowInterstitialAd(intersShowSuccessCallback);
        Debug.Log("Show Iron source interstitial");
        m_InterstitialAdShowSuccessCallback = intersShowSuccessCallback;
        MaxSdk.ShowInterstitial(m_InterstitialAdUnitID);
    }
    public override bool IsInterstitialLoaded()
    {
        return MaxSdk.IsInterstitialReady(m_InterstitialAdUnitID);
    }
    void OnInterstitialLoadedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got InterstitialAdReadyEvent");
        AnalysticManager.Instance.LogAdsInterLoadSuccess();
        AppsflyerTrackingManager.Instance.LogIntersAdLoaded();
    }

    void OnInterstitialFailedEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo)
    {
        Debug.Log("unity-script: I got InterstitialAdLoadFailedEvent, code: " + errorInfo.Code + ", description : " + errorInfo.Message);
        if (m_InterstitialAdLoadFailCallback != null)
        {
            m_InterstitialAdLoadFailCallback();
        }
        AnalysticManager.Instance.LogAdsInterLoadFail();
    }
    void InterstitialFailedToDisplayEvent(string adUnitId, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got InterstitialAdShowFailedEvent, code :  " + errorInfo.Code + ", description : " + errorInfo.Message);
        EventManager.TriggerEvent("TurnOffLoading");
    }
    void OnInterstitialDismissedEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Interstitial dismissed");
        if (m_InterstitialAdCloseCallback != null)
        {
            m_InterstitialAdCloseCallback();
        }
        EventManager.TriggerEvent("TurnOffLoading");
    }
    void OnInterstitialAdShowSucceededEvent(string adUnitId, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got InterstitialAdShowSuccee");
        AppsflyerTrackingManager.Instance.LogIntersAdStart();
    }
    #endregion

    #region Rewards
    public override void InitRewardVideoAd(UnityAction videoClosed, UnityAction videoLoadFailed)
    {
        base.InitRewardVideoAd(videoClosed, videoLoadFailed);
        Debug.Log("Init ironsource video");
        MaxSdkCallbacks.Rewarded.OnAdDisplayedEvent += RewardedVideoAdStartedEvent;
        MaxSdkCallbacks.Rewarded.OnAdDisplayFailedEvent += RewardedVideoAdShowFailedEvent;
        MaxSdkCallbacks.Rewarded.OnAdClickedEvent += RewardedVideoAdClickedEvent;
        MaxSdkCallbacks.Rewarded.OnAdReceivedRewardEvent += RewardedVideoAdRewardedEvent;
        MaxSdkCallbacks.Rewarded.OnAdHiddenEvent += RewardedVideoAdClosedEvent;
        MaxSdkCallbacks.Rewarded.OnAdLoadedEvent += Rewarded_OnAdLoadedEvent;
        RequestRewardVideoAd();
    }

    private void Rewarded_OnAdLoadedEvent(string adUnitID, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("Reward Ad Loaded");
        AppsflyerTrackingManager.Instance.LogRewardAdLoaded();
    }

    public override void RequestRewardVideoAd()
    {
        base.RequestRewardVideoAd();
        Debug.Log("Request ironsource Video");
        MaxSdk.LoadRewardedAd(m_RewardAdUnitID);
    }
    public override void ShowRewardVideoAd(UnityAction successCallback, UnityAction failedCallback)
    {
        base.ShowRewardVideoAd(successCallback, failedCallback);
        Debug.Log("Add Reward Ads Callback");
#if !UNITY_EDITOR
        Debug.Log("Show Video 1");
        m_IsWatchSuccess = false;
        MaxSdk.ShowRewardedAd(m_RewardAdUnitID);
        Debug.Log("Show Video 2");
#else
        m_IsWatchSuccess = false;
        RewardedVideoAdRewardedEvent("", new MaxSdkBase.Reward(), null);
#endif
    }
    public override bool IsRewardVideoLoaded()
    {
#if !UNITY_EDITOR
        return MaxSdk.IsRewardedAdReady(m_RewardAdUnitID);
#else
        return true;
#endif
    }

    /************* RewardedVideo Delegates *************/
    //void RewardedVideoAvailabilityChangedEvent(bool canShowAd) {
    //    Debug.Log("unity-script: I got RewardedVideoAvailabilityChangedEvent, value = " + canShowAd);
    //    if(!canShowAd) {
    //        EventManager.TriggerEvent("AdsLoadFail");
    //    } else {
    //        EventManager.TriggerEvent("AdsLoadSuccess");
    //    }
    //}

    void RewardedVideoAdRewardedEvent(string adUnitID, MaxSdkBase.Reward reward, MaxSdkBase.AdInfo adInfo)
    {
#if !UNITY_EDITOR
        Debug.Log("unity-script: I got RewardedVideoAdRewardedEvent");
#endif
        m_IsWatchSuccess = true;
        AppsflyerTrackingManager.Instance.LogRewardAdShowCompleted();
        if (Application.platform == RuntimePlatform.Android)
        {
            if (m_WatchVideoSuccessCallback != null)
            {
                Debug.Log("Watch video Success Callback!");
                EventManager.AddEventNextFrame(m_WatchVideoSuccessCallback);
                m_WatchVideoSuccessCallback = null;
            }
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (m_WatchVideoSuccessCallback != null)
            {
                Debug.Log("Watch video Success Callback!");
                EventManager.AddEventNextFrame(m_WatchVideoSuccessCallback);
                m_WatchVideoSuccessCallback = null;
            }
        }
    }
    void RewardedVideoAdClosedEvent(string adUnitID, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got RewardedVideoAdClosedEvent");
        if (m_WatchVideoSuccessCallback != null && m_IsWatchSuccess)
        {
            Debug.Log("Do Callback Success");
            EventManager.AddEventNextFrame(m_WatchVideoSuccessCallback);
            m_WatchVideoSuccessCallback = null;
        }
        else
        {
            Debug.Log("Don't have any callback");
        }
        if (m_CloseRewardVideoCallback != null)
        {
            m_CloseRewardVideoCallback();
        }
    }
    void RewardedVideoAdStartedEvent(string adUnitID, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got RewardedVideoAdStartedEvent");
        AppsflyerTrackingManager.Instance.LogRewardAdStart();
    }
    void RewardedVideoAdEndedEvent()
    {
        Debug.Log("unity-script: I got RewardedVideoAdEndedEvent");
        m_IsWatchSuccess = true;
    }
    void RewardedVideoAdShowFailedEvent(string adUnitID, MaxSdkBase.ErrorInfo errorInfo, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got RewardedVideoAdShowFailedEvent, code :  " + errorInfo.Code + ", description : " + errorInfo.Message);
        if (m_LoadRewardVideoFailedCallback != null)
        {
            m_LoadRewardVideoFailedCallback();
        }
    }
    void RewardedVideoAdClickedEvent(string adUnitID, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got RewardedVideoAdClickedEvent");
    }
    #endregion

    #region Banner
    public override void InitBannerAds()
    {
        base.InitBannerAds();
        Debug.Log("Ironsource Init Banner");
        MaxSdk.CreateBanner(m_BannerAdUnitID, MaxSdkBase.BannerPosition.BottomCenter);
        MaxSdk.SetBannerBackgroundColor(m_BannerAdUnitID, Color.black);
        MaxSdkCallbacks.Banner.OnAdLoadedEvent += BannerAdLoadedEvent;
    }
    public override void ShowBannerAds()
    {
        base.ShowBannerAds();
        MaxSdk.ShowBanner(m_BannerAdUnitID);
    }
    void BannerAdLoadedEvent(string adUnitID, MaxSdkBase.AdInfo adInfo)
    {
        Debug.Log("unity-script: I got BannerAdLoadedEvent");
        ShowBannerAds();
    }

    //void BannerAdLoadFailedEvent(IronSourceError error) {
    //    Debug.Log("unity-script: I got BannerAdLoadFailedEvent, code: " + error.getCode() + ", description : " + error.getDescription());
    //}

    //void BannerAdClickedEvent() {
    //    Debug.Log("unity-script: I got BannerAdClickedEvent");
    //    IronSource.Agent.displayBanner();
    //}

    //void BannerAdScreenPresentedEvent() {
    //    Debug.Log("unity-script: I got BannerAdScreenPresentedEvent");
    //}

    //void BannerAdScreenDismissedEvent() {
    //    Debug.Log("unity-script: I got BannerAdScreenDismissedEvent");
    //}

    //void BannerAdLeftApplicationEvent() {
    //    Debug.Log("unity-script: I got BannerAdLeftApplicationEvent");
    //}
    #endregion

}