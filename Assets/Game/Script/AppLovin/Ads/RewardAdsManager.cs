﻿using Firebase.RemoteConfig;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// TODO: +++++ Add new reward ads
public enum WatchVideoRewardType {
    NONE = 0,
    BONUS_COIN = 1,
    OWN_WEAPON = 2,
    UPGRADE_PLAYER = 3,
}
public class RewardAdsManager : Singleton<RewardAdsManager> {

    public double m_MaxRewardCooldownTime = 0;
    public double m_MaxSkipRewardCooldownTime = 0;

    public double m_InterstitialCooldownTime = 15;
    public int m_StartRewardLevel = 5;
    public int m_StartInterstitalLevel = 3;

    private double m_CurrentCooldownTime = 0;
    public double m_CurrentInterstitialCooldownTime = 15;
    private int m_IgnoreRewardTime = 0;
    public UnityAction m_RewardedSuccessCallback = null;
    public WatchVideoRewardType m_WatchType;
    public bool m_IgnoreGoogleAds;
    private void Awake() {
        DontDestroyOnLoad(gameObject);
        EventManager.StartListening("UpdateRemoteConfigs", UpdateRemoteConfigs);
    }
    public void Init() {
        m_CurrentCooldownTime = 0;
        m_CurrentInterstitialCooldownTime = 15;
        ResetIgnoreRewardTime();
    }
    public void Setup(double rewardCooldownTime) {
        m_MaxRewardCooldownTime = rewardCooldownTime;
        RefreshCooldown();
        ResetIgnoreRewardTime();
    }
    private void UpdateRemoteConfigs() {
        // TODO: Add Keys interstitial_time
        //ConfigValue configValue = FirebaseManager.Instance.GetConfigValue(Keys.key_remote_interstitial_time);
        //m_InterstitialCooldownTime = configValue.DoubleValue;
        //Debug.Log("Cooldown " + m_InterstitialCooldownTime);
    }
    private void Update() {
        if(m_CurrentCooldownTime > 0) {
            m_CurrentCooldownTime -= Time.deltaTime;
            if(m_CurrentCooldownTime <= 0) {
                EventManager.TriggerEvent("CheckBonus");
            }
        }
        if(m_CurrentInterstitialCooldownTime > 0) {
            m_CurrentInterstitialCooldownTime -= Time.deltaTime;
        }
    }
    public void RefreshCooldown() {
        m_CurrentCooldownTime = m_MaxRewardCooldownTime;
    }
    public void RefreshSkipCooldown() {
        m_CurrentCooldownTime = m_MaxSkipRewardCooldownTime;
    }
    public void RefreshCooldownInterstital() {
        m_CurrentInterstitialCooldownTime = m_InterstitialCooldownTime;
    }
    public void IgnoreReward() {
        m_IgnoreRewardTime++;
    }
    public void ResetIgnoreRewardTime() {
        m_IgnoreRewardTime = 0;
    }
    public int GetIgnoreRewardTime() {
        return m_IgnoreRewardTime;
    }
    public bool IsOnCappingTime() {
        if(m_CurrentInterstitialCooldownTime > 0) {
            return true;
        } else {
            return false;
        }
    }
    public bool IsGoodToShowInterstitial() {
#if UNITY_EDITOR
        return false;
#endif
        return AdsManager.Instance.IsInterstitialAdLoaded();
    }
    public bool IsGoodToShowReward() {
        if(m_CurrentCooldownTime > 0) return false;
        return true;
    }
    public void ShowRewardAds(WatchVideoRewardType watchType, UnityAction successCallback, UnityAction failCallback = null) {
        AppsflyerTrackingManager.Instance.LogRewardAdClickToShow();

        if (!IsGoodToShowReward()) return;
        m_RewardedSuccessCallback = successCallback;
        m_WatchType = watchType;

#if UNITY_EDITOR
        OnRewardedSuccessCallback();
#else
        if (AdsManager.Instance.IsRewardVideoLoaded()) {
            Debug.Log("Start Show Reward");
            AdsManager.Instance.ShowRewardVideo(OnRewardedSuccessCallback, failCallback);
        } 
#endif
        ResetIgnoreRewardTime();
        RefreshCooldownInterstital();
    }
    private void OnRewardedSuccessCallback() {
        if(m_RewardedSuccessCallback != null) {
            m_RewardedSuccessCallback();
        }
        AnalysticManager.Instance.LogWatchVideo(m_WatchType);

    }
    public bool IsRewardAdsLoaded() {
        if(AdsManager.Instance == null) return false;
        return AdsManager.Instance.IsReadyToShowRewardVideo();
    }
    public bool ShowInterstitial(UnityAction closeCallback, UnityAction successCallback, bool IsSkipCappingTime = false) {
        if(IsOnCappingTime() && !IsSkipCappingTime) {
            ExecuteCloseCallback(closeCallback);
            return false;
        }

        AnalysticManager.Instance.LogAdsInterClickShow();
        AppsflyerTrackingManager.Instance.LogIntersAdClickToShow();
        if(IsGoodToShowInterstitial()) {
            AnalysticManager.Instance.LogShowInterstital();
            EventManager.TriggerEvent("TurnOnLoading");
            AdsManager.Instance.ShowInterstitial(closeCallback, () => {
                if(successCallback != null) {
                    successCallback();
                }
            });
            RefreshCooldownInterstital();
            return true;
        }
        AnalysticManager.Instance.LogAdsInterShowFail();
        ExecuteCloseCallback(closeCallback);
        return false;
    }
    public void ExecuteCloseCallback(UnityAction closeCallback) {
        if(closeCallback != null) {
            closeCallback();
        }
    }
    public bool IsInterstitialLoaded() {
        return AdsManager.Instance.IsReadyToShowInterstitial();
    }
}