using Firebase.Analytics;
using Firebase.RemoteConfig;
using UnityEngine;
using UnityEngine.Events;

public class FirebaseManager : Singleton<FirebaseManager>
{
    private bool m_IsLoaded = false;
    public FirebaseAnalyticsManager m_FirebaseAnalyticsManager;
    public FirebaseRemoteConfigManager m_FirebaseRemoteConfigManager;

    private Firebase.DependencyStatus m_DependencyStatus = Firebase.DependencyStatus.UnavailableOther;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Init();
    }
    public void Init()
    {
        m_FirebaseAnalyticsManager = new FirebaseAnalyticsManager();
        m_FirebaseRemoteConfigManager = new FirebaseRemoteConfigManager();

        Debug.Log("Start Config");
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            if (task.Result == Firebase.DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + task.Result);
            }
        });
    }
    private void InitializeFirebase()
    {
        m_IsLoaded = true;
        m_FirebaseRemoteConfigManager.SetupDefaultConfigs();
        FetchData(() =>
        {
            Debug.Log("Fetch Success");
            EventManager.TriggerEvent("UpdateRemoteConfigs");
        });
    }
    public bool IsFirebaseReady()
    {
        return m_IsLoaded;
    }
    public void LogAnalyticsEvent(string eventName, string eventParamete, double eventValue)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName, eventParamete, eventValue);
        }
    }
    public void LogAnalyticsEvent(string eventName, Parameter[] paramss)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName, paramss);
        }
    }
    public void LogAnalyticsEvent(string eventName)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.LogEvent(eventName);
        }
    }
    public void SetUserProperty(string propertyName, string property)
    {
        if (IsFirebaseReady())
        {
            m_FirebaseAnalyticsManager.SetUserProperty(propertyName, property);
        }
    }
    public void FetchData(UnityAction successCallback)
    {
        m_FirebaseRemoteConfigManager.FetchData(successCallback);
    }
    public ConfigValue GetConfigValue(string key)
    {
        return m_FirebaseRemoteConfigManager.GetValues(key);
    }
}
