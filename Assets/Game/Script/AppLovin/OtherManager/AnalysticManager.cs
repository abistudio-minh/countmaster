using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;

public class AnalysticManager : Singleton<AnalysticManager>
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    public void LogWatchVideo(WatchVideoRewardType type)
    {
        string eventName = "ads_reward_show";
        Parameter[] parameters = new Parameter[] {
            new Parameter("type",type.ToString()),
        };
        FirebaseManager.Instance.LogAnalyticsEvent(eventName, parameters);
    }
    public void LogShowInterstital()
    {
        string eventName = "ad_inter_show";
        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
    public void LogPlayLevel(int level)
    {
        string eventName = "level_start";
        string s = level.ToString();
        Parameter[] parameters = new Parameter[] {
            new Parameter("level",s),
        };
        FirebaseManager.Instance.LogAnalyticsEvent(eventName, parameters);
    }
    public void LogWinLevel(int level)
    {
        string eventName = "level_complete";
        string s = level.ToString();
        Parameter[] parameters = new Parameter[] {
            new Parameter("level",s),
        };
        FirebaseManager.Instance.LogAnalyticsEvent(eventName, parameters);
    }
    public void LogLoseLevel(int level)
    {
        string eventName = "level_fail";
        string s = level.ToString();
        Parameter[] parameters = new Parameter[] {
            new Parameter("level",s),
        };
        FirebaseManager.Instance.LogAnalyticsEvent(eventName, parameters);
    }
    public void LogAdsInterLoadFail()
    {
        string eventName = "ad_inter_fail";
        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
    public void LogAdsInterLoadSuccess()
    {
        string eventName = "ad_inter_load";
        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
    public void LogAdsInterClickShow()
    {
        string eventName = "ad_inter_click";
        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
    public void LogAdsInterShowFail()
    {
        string eventName = "ad_inter_show_fail";
        FirebaseManager.Instance.LogAnalyticsEvent(eventName);
    }
}