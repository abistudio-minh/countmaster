using System.Collections.Generic;
using UnityEngine;
using AppsFlyerSDK;

public class AppsflyerTrackingManager : Singleton<AppsflyerTrackingManager>
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    public void LogCompleteTutorial()
    {
        SendEvent("af_tutorial_completion");
    }
    public void LogLevelAchieved(int level)
    {
        string eventName = "af_level_achieved";
        Dictionary<string, string> values = new Dictionary<string, string>();
        values.Add("version", Application.version);
        values.Add("af_level", level.ToString());
        SendEvent(eventName, values);
    }
    public void LogIntersAdClickToShow()
    {
        SendEvent("af_inters_ad_eligible");
    }
    public void LogIntersAdLoaded()
    {
        SendEvent("af_inters_api_called");
    }
    public void LogIntersAdStart()
    {
        SendEvent("af_inters_displayed");
    }
    public void LogRewardAdClickToShow()
    {
        SendEvent("af_rewarded_ad_eligible");
    }
    public void LogRewardAdLoaded()
    {
        SendEvent("af_rewarded_api_called");
    }
    public void LogRewardAdStart()
    {
        SendEvent("af_rewarded_displayed");
    }
    public void LogRewardAdShowCompleted()
    {
        SendEvent("af_rewarded_ad_completed");
    }
    private void SendEvent(string eventName)
    {
        Dictionary<string, string> values = new Dictionary<string, string>();
        values.Add("version", Application.version);
        SendEvent(eventName, values);
    }
    private void SendEvent(string eventName, Dictionary<string, string> eventValues)
    {
        AppsFlyer.sendEvent(eventName, eventValues);
    }
}
