using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Transform m_Transform;
    public Transform Transform
    {
        get
        {
            if (m_Transform == null)
            {
                m_Transform = transform;
            }
            return m_Transform;
        }
    }
    public float m_Duration;
    public int m_AttackDame;
    public float m_Speed;
    public Collider m_Collider;
    private void OnEnable()
    {
        GameManager.Instance.m_BulletList.Add(this);
    }
    private void Update()
    {
        Move();
    }
    private void OnTriggerEnter(Collider other)
    {
        OnHit(other);
    }
    public void Init(int dame, float speed)
    {
        m_AttackDame = dame;
        m_Speed = speed;
        StartCoroutine(SelfDespawn(3));
    }
    protected virtual void Move()
    {
        Transform.position += Transform.forward * m_Speed * Time.deltaTime;
    }
    IEnumerator SelfDespawn(float time)
    {
        yield return new WaitForSeconds(time);
        SimplePool.Despawn(gameObject);

        OnDespawn();
    }
    protected virtual void OnHit(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            other.GetComponent<EnemyCharacter>().TakeDame(m_AttackDame);
            OnDespawn();
        }

    }
    protected virtual void OnDespawn()
    {
        GameManager.Instance.m_BulletList.Remove(this);
        SimplePool.Despawn(gameObject);

    }
}
