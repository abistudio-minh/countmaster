using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public StateMachine<Character> m_MainStateMachine;
    private Transform m_Transform;
    public Transform Transform
    {
        get
        {
            if (m_Transform == null)
            {
                m_Transform = transform;
            }
            return m_Transform;
        }
    }
    #region Init Functions
    public virtual void Init()
    {
        InitStateMachine();
    }
    public void InitStateMachine()
    {
        m_MainStateMachine = new StateMachine<Character>(this);
        m_MainStateMachine.SetCurrentState(CharacterIdleState.Instance);
        m_MainStateMachine.SetGlobalState(CharacterGlobalState.Instance);
    }
    #endregion

    #region State Machine Functions
    public virtual void OnRunning()
    {

    }
    public virtual void OnIdleStart()
    {

    }
    public virtual void OnIdleExecute()
    {

    }
    public virtual void OnIdleExit()
    {

    }
    public virtual void OnMoveFollowGroupStart()
    {

    }
    public virtual void OnMoveFollowGroupExecute()
    {

    }
    public virtual void OnMoveFollowGroupExit()
    {

    }
    public virtual void OnMoveToLineStart()
    {

    }
    public virtual void OnMoveToLineExecute()
    {

    }
    public virtual void OnMoveToLineExit()
    {

    }
    public virtual void OnStandAttackStart()
    {

    }
    public virtual void OnStandAttackExecute()
    {

    }
    public virtual void OnStandAttackExit()
    {

    }
    public virtual void OnDeadStart()
    {

    }
    public virtual void OnDeadExecute()
    {

    }
    public virtual void OnDeadExit()
    {

    }
    #endregion



}
