using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDeadState : State<Character>
{
    private static CharacterDeadState m_Instance = null;
    public static CharacterDeadState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterDeadState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnDeadStart();
    }

    public override void Execute(Character go)
    {
        go.OnDeadExecute();
    }

    public override void Exit(Character go)
    {
        go.OnDeadExit();
    }
}
