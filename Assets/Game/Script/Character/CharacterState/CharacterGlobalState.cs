using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGlobalState : State<Character>
{
    private static CharacterGlobalState m_Instance = null;
    public static CharacterGlobalState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterGlobalState();
            }
            return m_Instance;
        }
    }

    public override void Enter(Character go)
    {
        return;
    }

    public override void Execute(Character go)
    {
        go.OnRunning();
    }

    public override void Exit(Character go)
    {
        return;
    }
}
