using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterIdleState : State<Character>
{
    private static CharacterIdleState m_Instance = null;
    public static CharacterIdleState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterIdleState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnIdleStart();
    }

    public override void Execute(Character go)
    {
        go.OnIdleExecute();
    }

    public override void Exit(Character go)
    {
        go.OnIdleExit();
    }
}
