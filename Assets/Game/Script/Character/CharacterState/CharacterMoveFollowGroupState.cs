using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveFollowGroupState : State<Character>
{
    private static CharacterMoveFollowGroupState m_Instance = null;
    public static CharacterMoveFollowGroupState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterMoveFollowGroupState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnMoveFollowGroupStart();
    }

    public override void Execute(Character go)
    {
        go.OnMoveFollowGroupExecute();
    }

    public override void Exit(Character go)
    {
        go.OnMoveFollowGroupExit();
    }
}
