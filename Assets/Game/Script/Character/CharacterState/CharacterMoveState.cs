using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveState : State<Character>
{
    private static CharacterMoveState m_Instance = null;
    public static CharacterMoveState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterMoveState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnMoveStart();
    }

    public override void Execute(Character go)
    {
        go.OnMoveExecute();
    }

    public override void Exit(Character go)
    {
        go.OnMoveExit();
    }
}
