using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMoveToLineState : State<Character>
{
    private static CharacterMoveToLineState m_Instance = null;
    public static CharacterMoveToLineState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterMoveToLineState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnMoveToLineStart();
    }

    public override void Execute(Character go)
    {
        go.OnMoveToLineExecute();
    }

    public override void Exit(Character go)
    {
        go.OnMoveToLineExit();
    }
}
