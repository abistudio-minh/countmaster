using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStandAttackState : State<Character>
{
    private static CharacterStandAttackState m_Instance = null;
    public static CharacterStandAttackState Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new CharacterStandAttackState();
            }
            return m_Instance;
        }
    }
    public override void Enter(Character go)
    {
        go.OnStandAttackStart();
    }

    public override void Execute(Character go)
    {
        go.OnStandAttackExecute();
    }

    public override void Exit(Character go)
    {
        go.OnStandAttackExit();
    }
}
