using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyCharacter : Character
{   
    public List<PlayerCharacter> m_CharacterList;
    private PlayerCharacter chosenOne;
    private Transform standPosition;
    private Transform groupPosition;
    private Transform closeZonePosition;
    //private float currentTime = 0;
    private float percentX;
    private Vector3 bufferPos = Vector3.zero;
    public float Debugpos;
    public NavMeshAgent agent;
    public int health ;
    public float minSpeed = 4.5f ;
    public float maxSpeed = 10f ;
    bool isDeath = false;
    public EnemyState currentState;
    public int filterPlayerLayer = 64;
    Collider m_Collider;
    public Vector3 m_CurrentDestination;
    public Animator m_Animator;
    private void Start() {
        m_CharacterList = GroupManager.Instance.m_CharacterList;
        standPosition = GroupManager.Instance.standHere;
        groupPosition = GroupManager.Instance.m_Group;
    }

    private void OnEnable() {
        if(m_Collider == null){
            m_Collider = GetComponent<Collider>();
        }
        isDeath = false;
        health = 2;
        currentState = EnemyState.GoStraight;
        m_Collider.enabled = true;
        m_CurrentDestination = Transform.position;
    }

    private void Update() {
        AdjustSpeed();
        if (currentState == EnemyState.GoStraight){
            MoveFoward();
        }else if(currentState == EnemyState.GoInto){
            MoveToGroup();
        }else if(currentState == EnemyState.FindTarget){
            MoveToPlayer();
        }
    }

    private void OnTriggerEnter(Collider other) {
        OnHit(other.tag);
    }
    public void TakeDame(int dame)
    {
        health -= dame;
        if (health <= 0) 
        {
            PrepareDie();
        }

    }
    public void OnHit(string tag)
    {   
        switch (tag) 
        {
            case "Trap":
                PrepareDie();
                break;
        }
    }

    public void PrepareDie(){
        m_Animator.SetBool("isDeath",true);
        m_Collider.enabled = false;
        agent.isStopped = true;
        Invoke("Die",1f);
    }

    public void Die(){
        if(isDeath)  return;
        isDeath = true;
        SimplePool.Despawn(gameObject);
        EnemyManager.Instance.KillEnemy(this);
    }

    void MoveFoward(){
        if (Vector3.Distance(groupPosition.position, Transform.position) < 5.5f)
        {
            currentState = EnemyState.GoInto;
        }
        Debugpos = Vector3.Distance(m_CurrentDestination, Transform.position);
        if (Vector3.Distance(m_CurrentDestination, Transform.position) < 0.5f)
        {
            m_CurrentDestination = groupPosition.position + groupPosition.right * percentX * 5;
            agent.SetDestination(m_CurrentDestination);
        }
    }

    void MoveToGroup(){
        float threshold = 1f;
        agent.SetDestination(standPosition.position);
        if(Vector3.Distance(transform.position,standPosition.position)<threshold){
            //Collider[] hitColliders = Physics.OverlapSphere(transform.position, 5f , filterPlayerLayer);
            currentState = EnemyState.FindTarget;
        }
    }

    void MoveToPlayer(){
        if(chosenOne == null || chosenOne.isActiveAndEnabled == false){
            if(m_CharacterList.Count == 0)  return;
            int randomChar = Random.Range(0,m_CharacterList.Count);
            chosenOne = m_CharacterList[randomChar];
        }
        //transform.position = Vector3.MoveTowards(transform.position,chosenOne.transform.position,minSpeed *Time.deltaTime);
        agent.SetDestination(chosenOne.transform.position);
    }

    void AdjustSpeed(){
        float distance = Vector3.Distance(transform.position,groupPosition.position);
        if(distance > 20f){
            agent.speed = maxSpeed;
        }else if(distance > 5f){
            agent.speed = minSpeed + (maxSpeed - minSpeed)* distance/30 ;
        }else{
            agent.speed = minSpeed;
        }
    }

    void SetXPos(float xPos){
        //xRandomPos = xPos;
        percentX = xPos/5;
    }
}

public enum EnemyState{
    GoStraight,
    GoInto,
    FindTarget
}