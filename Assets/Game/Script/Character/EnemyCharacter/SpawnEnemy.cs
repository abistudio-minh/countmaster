using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    public Transform spawnPlace;
    public GameObject enemyPrefab;
    public SpawnType spawnType;
    public int m_EnemySpawn;
    public int m_EnemyMinSpawn;
    public int m_EnemyMaxSpawn;

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "SpawnBox"){
            Spawn();
        }
    }

    void Spawn(){
        int quantity;
        if(spawnType == SpawnType.Random){
            quantity = Random.Range(m_EnemyMinSpawn,m_EnemyMaxSpawn);
        }else{
            quantity = m_EnemySpawn;
        }
        for(int i = 0 ; i<quantity;i++){
            float randomXSpawn = Random.Range(-4.5f,4.5f);
            Vector3 spawnPos = spawnPlace.position + spawnPlace.right * randomXSpawn;
            GameObject enemy = SimplePool.Spawn(enemyPrefab,spawnPos,Quaternion.identity);
            EnemyManager.Instance.m_EnemyList.Add(enemy);
            enemy.transform.LookAt(GroupManager.Instance.m_Group);
            enemy.SendMessage("SetXPos",randomXSpawn);
        }
    }

}

public enum SpawnType
{
    Random,
    NotRandom
}