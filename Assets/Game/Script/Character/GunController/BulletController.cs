using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    private Vector3 shootDirection;
    private float moveSpeed = 50f;

    //For debug

    // Update is called once per frame
    private void OnEnable() {
        StartCoroutine(SelfDestruct());
    }

    void Update()
    {
        transform.position += moveSpeed * transform.forward * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other) {
        if( other.tag == "Enemy"){
            SimplePool.Despawn(gameObject);
        }   
    }


    IEnumerator SelfDestruct()
    {
     yield return new WaitForSeconds(3f);
     SimplePool.Despawn(gameObject);
    }
}
