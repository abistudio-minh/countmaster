using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{   
    [SerializeField] private float fireRate = 1f;
    [SerializeField] private GameObject bulletPrefab;
    private float lastShotTime = 0f;
    private float startShotTime ;
    // Start is called before the first frame update
    void Start()
    {
        lastShotTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //lastShotTime = Time.time;
        while(lastShotTime < Time.time){
            Shoot();
            lastShotTime += 1/fireRate;
        }
    }

    void Shoot()
    {   
        Vector3 pos = gameObject.transform.position;
        GameObject bullet = SimplePool.Spawn(bulletPrefab,pos,gameObject.transform.rotation);
    }
}
