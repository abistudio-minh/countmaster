using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class PlayerCharacter : Character
{
    private Rigidbody m_Rigidbody;
    public StateMachine<PlayerCharacter> m_AttackStateMachine;
    public Animator m_Animator;
    public int m_Level;
    public float m_NormalScale;
    public Vector3 m_MoveDirection;
    public float m_MovementSpeed;
    public Position m_CurrentPosition;

    public Transform m_Model;
    [Header("Attack")]
    //private bool m_CanAttack;
    public WeaponData m_WeaponData;
    public Transform m_WeaponPosition;
    public Weapon m_CurrentWeapon;

    public float m_CurrentAttackTime;


    [Header("Test Upgrade Level")]
    public List<Color> m_UpgradeColorList;
    public SkinnedMeshRenderer m_ModelMesh;
    #region Unity Functions
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        
    }
    public void Start()
    {

    }
    private void Update()
    {
        m_MainStateMachine.Update();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            m_MainStateMachine.ChangeState(CharacterDeadState.Instance);
            other.GetComponent<EnemyCharacter>().PrepareDie();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {

    }

    #endregion
    #region Init Functions
    public override void Init()
    {
        base.Init();
        //InitTank();
    }
    //public void InitTank()
    //{
    //    m_TankData = PlayerData.Instance.m_CurrentTankData;
    //    if (m_CurrentTank != null)
    //    {
    //        SimplePool.Despawn(m_CurrentTank.gameObject);
    //    }
    //    m_CurrentTank = SimplePool.Spawn(m_TankData.tankObject, m_Model.position, m_Model.rotation).GetComponent<Tank>();
    //    m_CurrentTank.transform.parent = m_Model;
    //    m_CurrentTank.Init(m_TankData.tankBullet);
    //}

    #endregion
    #region State Machine Functions
    public override void OnRunning()
    {
        base.OnRunning();
        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    ChangeWeapon(WeaponManager.Instance.GetWeaponData("Gun"));
        //}
        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    ChangeWeapon(WeaponManager.Instance.GetWeaponData("Bomb"));
        //}
    }
    public override void OnIdleStart()
    {
        base.OnIdleStart();
        m_Animator.Play("Idle", 1, Random.Range(0.0f, 1.0f));
        ChangeWeapon(null);
        m_Rigidbody.velocity = Vector3.zero;

    }
    public override void OnIdleExecute()
    {
        base.OnIdleExecute();
        if (GroupManager.Instance.m_GroupMovementSpeed > 0)
        {
            m_MainStateMachine.ChangeState(CharacterMoveFollowGroupState.Instance);
        }
    }

    public override void OnIdleExit()
    {
        base.OnIdleExit();
    }
    public override void OnMoveFollowGroupStart()
    {
        base.OnMoveFollowGroupStart();
        m_Animator.Play("Move", 1, GroupManager.Instance.GetGlobalNormalizeTime(1));
        if (m_WeaponData == null)
        {
            ChangeWeapon(GroupManager.Instance.m_CurrentWeapon);
        }
    }
    public override void OnMoveFollowGroupExecute()
    {
        base.OnMoveFollowGroupExecute();
        MoveToPosition();
        //if (GroupManager.Instance.m_GroupMovementSpeed == 0)
        //{
        //    m_MainStateMachine.ChangeState(CharacterIdleState.Instance);
        //}
    }

    public override void OnMoveFollowGroupExit()
    {
        base.OnMoveFollowGroupExit();
    }
    public override void OnMoveToLineStart()
    {
        base.OnMoveToLineStart();
    }
    public override void OnMoveToLineExecute()
    {
        base.OnMoveToLineExecute();
        MoveToLine();
    }
    public override void OnMoveToLineExit()
    {
        base.OnMoveToLineExit();
    }
    public override void OnStandAttackStart()
    {
        base.OnStandAttackStart();
        m_Animator.Play("Stand", 1);
    }
    public override void OnStandAttackExecute()
    {
        base.OnStandAttackExecute();
    }
    public override void OnStandAttackExit()
    {
        base.OnStandAttackExit();
    }
    public override void OnDeadStart()
    {
        base.OnDeadStart();
        GroupManager.Instance.RemoveSinglePlayerCharacter(this);
    }
    public override void OnDeadExecute()
    {
        base.OnDeadExecute();
    }
    public override void OnDeadExit()
    {
        base.OnDeadExit();
    }
    #endregion
    #region Move Functions
    public void MoveToPosition()
    {
        if (m_CurrentPosition == null) return;

        m_MoveDirection = m_CurrentPosition.transform.position - Transform.position;
        float dis = m_MoveDirection.magnitude;
        if (dis > 0.2f)
        {
            float alpha = Mathf.Clamp(dis / 1, 1, 3);
            m_Rigidbody.velocity = m_MoveDirection.normalized * m_MovementSpeed * alpha;
        }
        else
        {
            m_Rigidbody.velocity = Vector3.zero;
            Transform.position = m_CurrentPosition.transform.position;
        }

    }
    public void MoveToLine()
    {
        if (m_CurrentPosition == null) return;

        m_MoveDirection = m_CurrentPosition.transform.position - Transform.position;
        float dis = m_MoveDirection.magnitude;
        if (dis > 0.2f)
        {
            float alpha = Mathf.Clamp(dis / 1, 1, 3);
            m_Rigidbody.velocity = m_MoveDirection.normalized * m_MovementSpeed * alpha;
        }
        else
        {
            m_Rigidbody.velocity = Vector3.zero;
            Transform.position = m_CurrentPosition.transform.position;
            m_MainStateMachine.ChangeState(CharacterStandAttackState.Instance);
        }
    }
    #endregion
    #region Other Functions
    public void SetPosition(Position pos)
    {
        m_CurrentPosition = pos;
    }
    public void ChangeToMoveState()
    {
        m_MainStateMachine.ChangeState(CharacterMoveFollowGroupState.Instance);
    }
    public void ChangeToStandAttackState()
    {
        m_MainStateMachine.ChangeState(CharacterMoveToLineState.Instance);
    }
    public void ChangeToIdleState()
    {
        m_MainStateMachine.ChangeState(CharacterIdleState.Instance);
    }
    public void SetNormalScale(float value)
    {
        m_NormalScale = value;
        Transform.localScale = Vector3.one * m_NormalScale;
    }
    public void IncreaseLevel()
    {
        m_Level++;
        m_ModelMesh.material.color = m_UpgradeColorList[m_Level];
    }
    public void DecreaseLevel()
    {
        m_Level--;
        m_ModelMesh.material.color = m_UpgradeColorList[m_Level];
    }
    public void SetLevel(int value)
    {
        m_Level = value;
        m_ModelMesh.material.color = m_UpgradeColorList[m_Level];
    }
    public void ChangeWeapon(WeaponData weaponData)
    {
        DespawnCurrentWeapon();

        m_WeaponData = weaponData;
        if (weaponData == null)
        {
            m_Animator.Play("Idle", 2, Random.Range(0.0f, 1.0f));
        }
        else
        {
            m_Animator.Play(m_WeaponData.weaponAnimationString, 2, Random.Range(0f, 1f));
            m_CurrentWeapon = SimplePool.Spawn(m_WeaponData.weaponObject, m_WeaponPosition.position, m_WeaponPosition.rotation).GetComponent<Weapon>();
            m_CurrentWeapon.transform.parent = m_WeaponPosition;
            m_CurrentWeapon.Init(m_WeaponData.weaponBulletDamage, m_WeaponData.weaponBulletMovementSpeed, m_WeaponData.weaponBullet);
        }
    }
    public void DespawnCurrentWeapon()
    {
        if (m_CurrentWeapon != null)
        {
            SimplePool.Despawn(m_CurrentWeapon.gameObject);
            m_CurrentWeapon = null;
        }
    }
    public void Attack()
    {
        m_CurrentWeapon?.Attack();
    }

    #endregion
}
