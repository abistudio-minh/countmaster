using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacterAnimationTrigger : MonoBehaviour
{
    public PlayerCharacter m_PlayerCharacter;
    public void Attack()
    {
        m_PlayerCharacter.Attack();
    }
}
