using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keys
{
    public static string key_current_start_weapon = "key_current_start_weapon";
    public static string key_current_tank = "key_current_tank";
    public static string key_current_level = "key_current_level";
    public static string key_current_manpower_upgrade = "key_current_manpower_upgrade";
    public static string key_current_income_upgrade = "key_current_income_upgrade";
    public static string key_current_damage_upgrade = "key_current_damage_upgrade";
    public static string key_current_coin = "key_current_coin";
    public static string key_unlock_weapon = "key_unlock_weapon";
    public static string key_own_weapon = "key_own_weapon";
    public static string key_current_unlock_weapon = "key_current_unlock_weapon";
    public static string key_current_unlock_weapon_process = "key_current_unlock_weapon_process";
}
