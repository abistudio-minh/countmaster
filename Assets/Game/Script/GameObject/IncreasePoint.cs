using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
public class IncreasePoint : MonoBehaviour
{
    public bool m_IsActive;
    public IncreaseType m_Type;
    public int m_ValueIncrease;
    public BoxCollider m_Collider;
    public TextMeshPro m_Text;
    public UnityAction m_OnIncrease;

    public void Awake()
    {
        m_IsActive = true;
        m_Text.text = string.Format("{0}{1}", m_Type == IncreaseType.ADDITION ? "+" : "x", m_ValueIncrease.ToString());
        m_OnIncrease += InCrease;
    }
    public void OnTriggerEnter(Collider other)
    {
        m_OnIncrease();
    }
    public void InCrease()
    {
        if (m_IsActive)
        {
            switch (m_Type)
            {
                case IncreaseType.ADDITION:
                    GroupManager.Instance.AddPlayerCharacter(m_ValueIncrease);
                    break;
                case IncreaseType.MULTIPLICATION:
                    GroupManager.Instance.MultiplyPlayerCharacter(m_ValueIncrease);
                    break;
            }
        }
    }
    public void SetActive(bool value)
    {
        m_IsActive = value;
    }
}

public enum IncreaseType
{
    ADDITION,
    MULTIPLICATION
}
