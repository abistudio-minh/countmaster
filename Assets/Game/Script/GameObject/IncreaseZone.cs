using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
public class IncreaseZone : MonoBehaviour
{
    public List<IncreasePoint> points = new List<IncreasePoint>();

    private void Start()
    {
        for (int i = 0; i < points.Count; i++)
        {
            points[i].m_OnIncrease += DeactiveAllInCreasePoint;
        }
    }
    public void DeactiveAllInCreasePoint()
    {
        for (int i = 0; i < points.Count; i++)
        {
            points[i].SetActive(false);
        }
        transform.DOMoveY(-4, 2);
    }
}

