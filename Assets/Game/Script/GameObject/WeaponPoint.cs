using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPoint : MonoBehaviour
{
    private bool m_IsActive;
    public WeaponData m_WeaponData;
    private void Awake()
    {
        m_IsActive = true;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (m_IsActive)
        {
            m_IsActive = false;
            Debug.Log(m_WeaponData.weaponName);
            GroupManager.Instance.ChangeGroupWeapon(m_WeaponData);
            Destroy(gameObject);
        }
    }
}
