using System;
using UnityEngine;

public class CameraManager : Singleton<CameraManager>
{
    private Transform m_Transform;
    public Transform Transform
    {
        get
        {
            if (m_Transform == null)
            {
                m_Transform = transform;
            }
            return m_Transform;
        }
    }
    public Camera m_MainCamera => Camera.main;

}
