using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager: Singleton<EnemyManager>
{
    public int enemyCount { get => m_EnemyList.Count; }
    public int enemyKill;
    public List<GameObject> m_EnemyList;
    private void Awake()
    {
        ResetEnemyKill();
    }
    public void RemoveAllEnemy()
    {
        for (int i = 0; i < enemyCount; i++)
        {
            SimplePool.Despawn(m_EnemyList[i]);
        }
        m_EnemyList.Clear();
        enemyKill = 0;
    }
    public void ResetEnemyKill()
    {
        enemyKill = 0;
    }
    public void KillEnemy(EnemyCharacter enemy)
    {
        enemyKill++;
        m_EnemyList.Remove(enemy.gameObject);
    }
}
