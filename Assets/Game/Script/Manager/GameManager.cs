using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameManager : Singleton<GameManager>
{
    public bool m_IsStartGame;
    public bool m_IsPreEndGame;
    public bool m_IsStartCheckWinGame;

    public GameObject m_CurrentLevel;

    public List<Projectile> m_BulletList;
    #region Unity Functions
    private void Awake()
    {
        m_IsStartGame = false;
        m_IsPreEndGame = false;
        m_IsStartCheckWinGame = false;
    }
    private void Start()
    {
        InitGame();
        // TODO: +++++ Test banner ads
        //AdsManager.Instance.ShowBannerAds();
    }
    private void Update()
    {
        CheckLoseGame();
        CheckWinGame();
    }
    #endregion
    #region Event Functions
    public void InitGame()
    {
        GroupManager.Instance.RemoveAllPlayerCharacter();
        GroupManager.Instance.AddPlayerCharacter(PlayerData.Instance.m_CurrentManPowerUpgrade, false);
        
        LoadMapLevel(PlayerData.Instance.m_CurrentLevel);
    }
    public void OnStartGame()
    {
        m_IsStartGame = true;
        PlayerInputManager.Instance.m_CanMoveGroup = true;

        // TODO: +++++ Change movement speed
        GroupManager.Instance.m_GroupMovementSpeed = 3;
        GroupManager.Instance.InitGroupWeapon();
        GameUIManager.Instance.m_PanelMainMenu.OnDeactive();

        // TODO: Check later
        GameUIManager.Instance.m_BannerCharacterCount.gameObject.SetActive(true);

        AnalysticManager.Instance.LogPlayLevel(PlayerData.Instance.m_CurrentLevel);
    }
    public void OnPreEndGame()
    {
        m_IsPreEndGame = true;
        PlayerInputManager.Instance.m_CanMoveGroup = false;

        // TODO: Check later
        GameUIManager.Instance.m_BannerCharacterCount.gameObject.SetActive(false);
    }
    public void OnLoseGame()
    {
        m_IsStartGame = false;
        m_IsPreEndGame = false;
        PlayerInputManager.Instance.m_CanMoveGroup = false;
        GroupManager.Instance.m_GroupMovementSpeed = 0;

        GameUIManager.Instance.m_PanelEndGame.Setup(false);
        GameUIManager.Instance.m_PanelEndGame.OnActive();

        AnalysticManager.Instance.LogLoseLevel(PlayerData.Instance.m_CurrentLevel);
    }
    public void OnWinGame()
    {
        m_IsStartGame = false;
        m_IsPreEndGame = false;
        GroupManager.Instance.ChangeGroupToIdleState();
        GameUIManager.Instance.m_PanelEndGame.Setup(true);
        GameUIManager.Instance.m_PanelEndGame.OnActive();

        AnalysticManager.Instance.LogWinLevel(PlayerData.Instance.m_CurrentLevel);

        PlayerData.Instance.IncreaseLevel();
    }
    public void OnRestartGame()
    {
        for (int i = 0; i < m_BulletList.Count; i++)
        {
            SimplePool.Despawn(m_BulletList[i].gameObject);
        }
        m_BulletList.Clear();
        EnemyManager.Instance.RemoveAllEnemy();
        EnemyManager.Instance.ResetEnemyKill();
        PlayerInputManager.Instance.ClearInput();
        
        InitGame();

        GameUIManager.Instance.m_PanelEndGame.OnDeactive();
        GameUIManager.Instance.m_PanelMainMenu.OnActive();
    }
    public void DelayAction(UnityAction action, float time)
    {
        StartCoroutine(CoDelayAction(action, time));
    }
    IEnumerator CoDelayAction(UnityAction action, float time)
    {
        yield return new WaitForSeconds(time);
        action?.Invoke();
    }
    #endregion
    #region Other Functions
    public void CheckLoseGame()
    {
        if (m_IsStartGame && GroupManager.Instance.m_CharacterCount == 0)
        {
            OnLoseGame();
        }
    }
    public void CheckWinGame()
    {
        if (m_IsPreEndGame && EnemyManager.Instance.enemyCount == 0)
        {
            OnWinGame();
        }
    }
    public void AddClickEvent(Button button, UnityAction callback)
    {
        button.onClick.AddListener(() => {
            // TODO: +++++ Add Sound Click Button
            if (callback != null)
            {
                callback();
            }
        });
    }
    public void LoadMapLevel(int level)
    {
        if (m_CurrentLevel!= null)
        {
            Destroy(m_CurrentLevel);
        }
        try
        {
            m_CurrentLevel = Instantiate(Resources.Load<GameObject>(string.Format("Level/Level_{0}", level.ToString())));
            GroupManager.Instance.GeneratePath(m_CurrentLevel.GetComponent<LevelManager>().GetPointList());
            GroupManager.Instance.InitGroupToStartPosition();
        }
        catch (System.Exception)
        {
            Debug.Log(string.Format("Can't generate level {0} (Level_{0} = null)", level));
        }
    }
    #endregion

    
}
