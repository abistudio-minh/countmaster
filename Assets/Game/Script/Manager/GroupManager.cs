using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
public class GroupManager : Singleton<GroupManager>
{
    [Header("Group Movement")]
    public Transform m_Group;
    public Transform standHere;
    public float m_GroupMovementSpeed;
    [Header("Player Character")]
    public WeaponData m_CurrentWeapon;
    public Transform m_StandHere;
    public GameObject m_PlayerCharacterPrefab;

    public int m_MaxCharacterCount;
    public int m_GroupCount;
    public int m_CharacterCount { get { return m_CharacterList.Count; } }
    public List<PlayerCharacter> m_CharacterList = new List<PlayerCharacter>();



    [Header("Character Hexa Position")]
    public Transform m_HexaGroup;
    public GameObject m_HexaPrefab;
    public int m_HexaRow;
    public int m_HexaCol;
    public float m_HexaScale;

    public List<Position> m_HexaPositionList = new List<Position>();
    public List<Position> m_PriorityHexaPositionList = new List<Position>();
    private Coroutine m_CoUpdateHexaPosition;

    [Header("Character Square Position")]
    public Transform m_SquareGroup;
    public GameObject m_SquarePrefab;
    public int m_SquareRow;
    public int m_SquareCol;
    public float m_SquareScale;

    public List<Position> m_SquarePositionList = new List<Position>();
    public List<Position> m_PrioritySquarePositionList = new List<Position>();

    [Header("Path Creator")]
    public float m_Distance;
    public PathCreator m_Creator;
    public List<Transform> m_Points;
    #region Unity Functions
    private void Awake()
    {
        InitGroupHexaPosition();
        GenerateHexaPriorityList();

        InitGroupSquarePosition();
        GenerateSquarePriorityList();
    }
    private void Update()
    {
        //UpdateGroupMovementSpeed();
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    AddPlayerCharacter(5);
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    RandomRemove(5);
        //    //res = GetAroundPosition(input.x, input.y);
        //    //res.Add(GetPosition(input.x, input.y));
        //    //GeneratePriorityList();
        //}
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    UpdateGroupHexaPosition();
        //}
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    m_GroupMovementSpeed = m_GroupMovementSpeed == 0 ? 3 : 0;
        //}
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    UpdateGroupSquarePosition();
        //    ChangeGroupToStandAttackState();
        //}
        MoveGroup();
        CheckEndPath();
    }
    #endregion
    #region Group Movement Functions
    public void InitGroupToStartPosition()
    {
        m_Distance = 0;
        m_StandHere.localPosition = Vector3.zero;
        m_Group.position = m_Creator.path.GetPointAtDistance(m_Distance, EndOfPathInstruction.Stop);
    }

    public void MoveGroup()
    {
        m_Distance += m_GroupMovementSpeed * Time.deltaTime;
        m_Group.position = m_Creator.path.GetPointAtDistance(m_Distance, EndOfPathInstruction.Stop);
        m_Group.rotation = Quaternion.LookRotation(m_Creator.path.GetDirectionAtDistance(m_Distance, EndOfPathInstruction.Stop) * -1, Vector3.up);

    }
    public void CheckEndPath()
    {
        if (m_Distance > m_Creator.path.length && GameManager.Instance.m_IsStartGame && !GameManager.Instance.m_IsPreEndGame)
        {
            UpdateGroupSquarePosition();
            ChangeGroupToStandAttackState();
            GameManager.Instance.OnPreEndGame();
        }
    }
    #endregion
    #region Group Hexa Position Functions
    public void InitGroupHexaPosition()
    {
        for (int i = 0; i < m_HexaCol; i++)
        {
            for (int j = 0; j < m_HexaRow; j++)
            {
                Vector3 pos = m_HexaGroup.position + (new Vector3(j % 2 == 0 ? i : i - 0.5f , 0, j * 0.87f ) - new Vector3(m_HexaRow/2 % 2 == 0 ? m_HexaCol/2 : m_HexaCol/2 - 0.5f, 0, m_HexaRow / 2 * 0.87f)) * m_HexaScale;
                GameObject temp = SimplePool.Spawn(m_HexaPrefab, pos, Quaternion.identity);
                temp.transform.localScale = Vector3.one * m_HexaScale;
                temp.transform.parent = m_HexaGroup;
                m_HexaPositionList.Add(new Position(i, j, temp.transform));
            }
        }
    }
    public Position GetHexaPosition(int x, int y)
    {
        if (x < 0 || x > m_HexaCol-1 || y < 0 || y > m_HexaRow-1) return null;
        return m_HexaPositionList[x * m_HexaRow + y];
    }
    public List<Position> GetAroundHexaPosition(int x, int y)
    {
        List<Position> list = new List<Position>();
        Vector2Int[] index = null;
        if (y%2 == 1)
        {
            index = new Vector2Int[6] { 
                new Vector2Int(x , y - 1), 
                new Vector2Int(x - 1, y - 1), 
                new Vector2Int(x - 1, y), 
                new Vector2Int(x + 1, y), 
                new Vector2Int(x, y +1), 
                new Vector2Int(x - 1, y + 1)
            };
        }
        else
        {
            index = new Vector2Int[6] {
                new Vector2Int(x + 1 , y - 1),
                new Vector2Int(x, y - 1),
                new Vector2Int(x - 1, y),
                new Vector2Int(x + 1, y),
                new Vector2Int(x + 1, y +1),
                new Vector2Int(x, y + 1)
            };
        }
        for (int i = 0; i < index.Length; i++)
        {
            Position temp = GetHexaPosition(index[i].x, index[i].y);
            if (temp != null)
            {
                list.Add(temp);
            }
        }
        return list;
    }
    public void GenerateHexaPriorityList()
    {
        m_PriorityHexaPositionList.Clear();
        m_PriorityHexaPositionList.Add(GetHexaPosition(m_HexaCol/2, m_HexaRow/2));
        int index = 0;
        int count = 0;
        while (count < m_HexaRow * m_HexaCol && index < m_PriorityHexaPositionList.Count )
        {
            List<Position> aroundPos = GetAroundHexaPosition(m_PriorityHexaPositionList[index].x, m_PriorityHexaPositionList[index].y);
            for (int i = 0;i < aroundPos.Count; i++)
            {
                if (!m_PriorityHexaPositionList.Contains(aroundPos[i]))
                {
                    m_PriorityHexaPositionList.Add(aroundPos[i]);
                    count++;
                }
            }
            
            index++;
        }
    }
    public PlayerCharacter GetLowestLevelPlayerCharacter()
    {
        PlayerCharacter res = m_CharacterList[0];
        for (int i = 1; i < m_CharacterCount; i++)
        {
            if (res.m_Level > m_CharacterList[i].m_Level)
            {
                res = m_CharacterList[i];
            }
        }
        return res;
    }
    public void UpdateGroupHexaPosition()
    {
        for (int i = 0; i < m_CharacterCount; i++)
        {
            m_CharacterList[i].SetPosition(m_PriorityHexaPositionList[i]);
            PlayerInputManager.Instance.UpdateClampValue(m_CharacterList[i]);
        }
    }
    #endregion
    #region Group Square Position Function
    public void InitGroupSquarePosition()
    {
        for (int i = 0; i < m_SquareCol; i++)
        {
            for (int j = 0; j < m_SquareRow; j++)
            {
                Vector3 pos = m_SquareGroup.position + (new Vector3(i, 0 , -j) - new Vector3(m_SquareCol / 2f - 0.5f, 0, 0)) * m_SquareScale;
                GameObject temp = SimplePool.Spawn(m_SquarePrefab, pos, Quaternion.identity);
                temp.transform.localScale = Vector3.one * m_SquareScale;
                temp.transform.parent = m_SquareGroup;
                m_SquarePositionList.Add(new Position(i, j, temp.transform));
            }
        }
    }
    public void GenerateSquarePriorityList()
    {
        for (int j = 0; j < m_SquareRow; j++)
        {
            for (int i = 0; i <= m_SquareCol/2; i++)
            {
                Position pos1 = GetSquarePosition(m_SquareCol / 2 -1 + i, j);
                Position pos2 = GetSquarePosition(m_SquareCol / 2 - i, j);

                if (!m_PrioritySquarePositionList.Contains(pos1) && pos1 != null)
                {
                    m_PrioritySquarePositionList.Add(pos1);
                }
                if (!m_PrioritySquarePositionList.Contains(pos2) && pos2 != null)
                {
                    m_PrioritySquarePositionList.Add(pos2);
                }
            }
        }
    }
    public Position GetSquarePosition(int x, int y)
    {
        if (x < 0 || x > m_SquareCol - 1 || y < 0 || y > m_SquareRow - 1) return null;
        return m_SquarePositionList[x * m_SquareRow + y];
    }
    public void UpdateGroupSquarePosition()
    {
        for (int i = 0; i < m_CharacterCount; i++)
        {
            m_CharacterList[i].SetPosition(m_PrioritySquarePositionList[i]);
        }
    }
    #endregion
    #region PlayerCharacter Functions
    public void AddPlayerCharacter(int value, bool isRandomRadius = true)
    {
        for (int i = 0; i < value; i++)
        {
            AddSinglePlayerCharacter(isRandomRadius);
        }
    }
    public void MultiplyPlayerCharacter(int value, bool isRandomRadius = true)
    {
        int count = m_CharacterCount;
        for (int i = 0; i < value - 1; i++)
        {
            AddPlayerCharacter(count, isRandomRadius);
        }
    }
    public void AddSinglePlayerCharacter(bool isRandomRadius = true)
    {
        if (m_CharacterCount < m_MaxCharacterCount)
        {
            Vector3 randomPosition = isRandomRadius ? new Vector3(Random.Range(-0.5f, 0.5f), m_Group.position.y, Random.Range(-0.5f, 0.5f)) : Vector3.zero;
            PlayerCharacter tempCharacter = SimplePool.Spawn(m_PlayerCharacterPrefab, m_PriorityHexaPositionList[m_CharacterCount].transform.position + randomPosition, Quaternion.identity).GetComponent<PlayerCharacter>();
            tempCharacter.Transform.parent = m_StandHere;
            tempCharacter.Transform.rotation = Quaternion.LookRotation(m_Group.forward, Vector3.up);
            tempCharacter.Init();
            tempCharacter.SetNormalScale(m_HexaScale);
            tempCharacter.SetPosition(m_PriorityHexaPositionList[m_CharacterCount]);
            tempCharacter.SetLevel(0);
            
            m_CharacterList.Add(tempCharacter);
            PlayerInputManager.Instance.UpdateClampValue(tempCharacter);
        }
        else
        {
            GetLowestLevelPlayerCharacter().IncreaseLevel();
        }
        m_GroupCount++;

        EventManager.TriggerEvent("ChangeCharacterCount");
    }
    public void RandomRemove(int count)
    {
        for (int i = 0; i < count; i++)
        {
            int ran = Random.Range(0, m_CharacterCount);
            RemoveSinglePlayerCharacter(m_CharacterList[ran]);
        }
    }
    public void RemoveSinglePlayerCharacter(PlayerCharacter playerCharacter)
    {
        m_GroupCount -= (playerCharacter.m_Level + 1);
        m_CharacterList.Remove(playerCharacter);
        SimplePool.Despawn(playerCharacter.gameObject);

        EventManager.TriggerEvent("ChangeCharacterCount");

        // TODO: +++++ Check delay update hexa position
        if (m_CoUpdateHexaPosition != null)
        {
            StopCoroutine(m_CoUpdateHexaPosition);
        }
        m_CoUpdateHexaPosition = StartCoroutine(DelayUpdateHexaGroupPosition(2));
    }
    public void RemoveAllPlayerCharacter()
    {
        while (m_CharacterCount != 0)
        {
            RemoveSinglePlayerCharacter(m_CharacterList[0]);
        }
    }
    public float GetGlobalNormalizeTime(int layer)
    {
        if (m_CharacterCount == 0)
        {
            return 0;
        }
        return m_CharacterList[0].m_Animator.GetCurrentAnimatorStateInfo(layer).normalizedTime;
    }
    public void ChangeGroupWeapon(WeaponData weaponData)
    {
        m_CurrentWeapon = weaponData;
        Debug.Log(weaponData?.weaponName);
        for (int i = 0; i < m_CharacterCount; i++)
        {
            m_CharacterList[i].ChangeWeapon(m_CurrentWeapon);
        }
    }
    public void InitGroupWeapon()
    {
        m_CurrentWeapon = PlayerData.Instance.m_CurrentStartWeapon;
    }
    public void ChangeGroupToStandAttackState()
    {
        m_GroupMovementSpeed = 0;
        for (int i = 0; i < m_CharacterCount; i++)
        {
            m_CharacterList[i].ChangeToStandAttackState();
        }
    }
    public void ChangeGroupToIdleState()
    {
        m_GroupMovementSpeed = 0;
        for (int i = 0; i < m_CharacterCount; i++)
        {
            m_CharacterList[i].ChangeToIdleState();
        }
    }
    IEnumerator DelayUpdateHexaGroupPosition(float time)
    {
        yield return new WaitForSeconds(time);
        UpdateGroupHexaPosition();
    }
    #endregion
    #region Path Creator Functions
    public void GeneratePath(List<Transform> points)
    {
        m_Points = points;
        if (m_Points.Count == 0) return;
        List<Vector3> controlPoints = new List<Vector3>();
        for (int i = 0; i < m_Points.Count; i+=2)
        {
            Vector3 direction = (m_Points[i].position - m_Points[i+1].position).normalized * 3;
            controlPoints.Add(m_Points[i].position + direction);
            controlPoints.Add(m_Points[i].position);
            controlPoints.Add(m_Points[i].position - direction);

            controlPoints.Add(m_Points[i+1].position + direction);
            controlPoints.Add(m_Points[i+1].position);
            controlPoints.Add(m_Points[i+1].position - direction);
        }
        controlPoints.RemoveAt(controlPoints.Count - 1);
        controlPoints.RemoveAt(0);

        BezierPath bezierPath = new BezierPath(m_Points.ToArray(), false, PathSpace.xz);
        bezierPath.ControlPointMode = BezierPath.ControlMode.Aligned;
        for (int i = 0; i < controlPoints.Count; i++)
        {
            bezierPath.SetPoint(i, controlPoints[i]);
        }
        m_Creator.bezierPath = bezierPath;
    }
    #endregion

}
[System.Serializable]
public class Position
{
    public int x;
    public int y;
    public Transform transform;

    public Position(int x, int y, Transform transform)
    {
        this.x = x;
        this.y = y;
        this.transform = transform;
    }
}
