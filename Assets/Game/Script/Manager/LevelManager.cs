using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public List<Transform> m_PointList;

    public void GeneratePointList()
    {
        Transform movepoint = null;
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name == "MovePoint")
            {
                movepoint = transform.GetChild(i);
                break;
            }
        }

        m_PointList = new List<Transform>();
        for (int i = 0; i < movepoint.childCount; i++)
        {
            m_PointList.Add(movepoint.GetChild(i));
        }
    }

    public List<Transform> GetPointList()
    {
        return m_PointList;
    }
}
