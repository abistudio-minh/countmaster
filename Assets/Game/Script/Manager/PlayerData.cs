using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
public class PlayerData : Singleton<PlayerData>
{
    public WeaponData m_CurrentStartWeapon;
    public int m_CurrentLevel;
    public int m_CurrentCoin;
    public int m_CurrentManPowerUpgrade;
    public int m_CurrentIncomeUpgrade;
    public int m_CurrentDamageUpgrade;
    public WeaponData m_CurrentUnlockWeapon;
    public float m_CurrentUnlockWeaponProgress;
    public List<string> m_UnlockWeapon;
    public List<string> m_OwnWeapon;
    private void Awake()
    {
        LoadCurrentWeapon();
        LoadCurrentCoin();
        LoadCurrentLevel();
        LoadCurrentManPowerUpgrade();
        LoadCurrentDamageUpgrade();
        LoadCurrentIncomeUpgrade();
        LoadUnlockWeapon();
        LoadOwnWeapon();
        LoadCurrentUnlockWeapon();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            PlayerPrefs.DeleteAll();
        }
    }
    #region Save Load Data
    public void SaveCurrentStartWeapon()
    {
        PlayerPrefs.SetString(Keys.key_current_start_weapon, m_CurrentStartWeapon.weaponName);
    }
    public void LoadCurrentWeapon()
    {
        m_CurrentStartWeapon = WeaponManager.Instance.GetWeaponData(PlayerPrefs.GetString(Keys.key_current_start_weapon, "Gun1"));
    }
    public void SaveCurrentManPowerUpgrade()
    {
        PlayerPrefs.SetInt(Keys.key_current_manpower_upgrade, m_CurrentManPowerUpgrade);
    }
    public void LoadCurrentManPowerUpgrade()
    {
        m_CurrentManPowerUpgrade = PlayerPrefs.GetInt(Keys.key_current_manpower_upgrade, 1);
    }
    public void SaveCurrentLevel()
    {
        PlayerPrefs.SetInt(Keys.key_current_level, m_CurrentLevel);
    }
    public void LoadCurrentLevel()
    {
        m_CurrentLevel = PlayerPrefs.GetInt(Keys.key_current_level, 1);
    }
    public void SaveCurrentCoin()
    {
        PlayerPrefs.SetInt(Keys.key_current_coin, m_CurrentCoin);
    }
    public void LoadCurrentCoin()
    {
        m_CurrentCoin = PlayerPrefs.GetInt(Keys.key_current_coin, 0);
    }
    public void SaveCurrentIncomeUpgrade()
    {
        PlayerPrefs.SetInt(Keys.key_current_income_upgrade, m_CurrentIncomeUpgrade);
    }
    public void LoadCurrentIncomeUpgrade()
    {
        m_CurrentIncomeUpgrade = PlayerPrefs.GetInt(Keys.key_current_income_upgrade, 1);
    }
    public void SaveCurrentDamageUpgrade()
    {
        PlayerPrefs.SetInt(Keys.key_current_damage_upgrade, m_CurrentDamageUpgrade);
    }
    public void LoadCurrentDamageUpgrade()
    {
        m_CurrentDamageUpgrade = PlayerPrefs.GetInt(Keys.key_current_damage_upgrade, 1);
    }
    public void SaveUnlockWeapon()
    {
        PlayerPrefs.SetString(Keys.key_unlock_weapon, string.Join(',', m_UnlockWeapon.ToArray()));
    }
    public void LoadUnlockWeapon()
    {
        m_UnlockWeapon = PlayerPrefs.GetString(Keys.key_unlock_weapon, "Gun1,Gun2").Split(',').ToList();
    }
    public void SaveOwnWeapon()
    {
        PlayerPrefs.SetString(Keys.key_own_weapon, string.Join(',', m_OwnWeapon.ToArray()));
    }
    public void LoadOwnWeapon()
    {
        m_OwnWeapon = PlayerPrefs.GetString(Keys.key_own_weapon, "Gun1").Split(',').ToList();
    }
    public void SaveCurrentUnlockWeapon()
    {
        PlayerPrefs.SetString(Keys.key_current_unlock_weapon, m_CurrentUnlockWeapon != null ? m_CurrentUnlockWeapon.weaponName : "null");
        PlayerPrefs.SetFloat(Keys.key_current_unlock_weapon_process, m_CurrentUnlockWeaponProgress);
    }
    public void LoadCurrentUnlockWeapon()
    {
        WeaponData randomWeaponData = WeaponManager.Instance.GetRandomLockWeapon();
        if (WeaponManager.Instance.GetWeaponData(PlayerPrefs.GetString(Keys.key_current_unlock_weapon, "Gun3")) == null && randomWeaponData != null)
        {
            PlayerPrefs.SetString(Keys.key_current_unlock_weapon, WeaponManager.Instance.GetRandomLockWeapon().weaponName);
        } 

        m_CurrentUnlockWeapon = WeaponManager.Instance.GetWeaponData(PlayerPrefs.GetString(Keys.key_current_unlock_weapon, randomWeaponData?.weaponName));
        m_CurrentUnlockWeaponProgress = PlayerPrefs.GetFloat(Keys.key_current_unlock_weapon_process, 0);
    }
    #endregion
    #region Change Data Functions
    public void ConsumeCoin(int value)
    {
        m_CurrentCoin -= value;
        EventManager.TriggerEvent("CoinChange");
        SaveCurrentCoin();
        
    }
    public void AddCoin(int value)
    {
        m_CurrentCoin += value;
        EventManager.TriggerEvent("CoinChange");
        SaveCurrentCoin();
    }
    public void IncreaseLevel()
    {
        m_CurrentLevel++;

        SaveCurrentLevel();
    }
    public void UnlockWeapon(string weaponName)
    {
        m_UnlockWeapon.Add(weaponName);
        SaveUnlockWeapon();
    }
    public void OwnWeapon(string weaponName)
    {
        m_OwnWeapon.Add(weaponName);
        SaveOwnWeapon();
    }
    public bool IsUnlockWeapon(string weaponName)
    {
        return m_UnlockWeapon.Contains(weaponName);
    }
    public bool IsOwnWeapon(string weaponName)
    {
        return m_OwnWeapon.Contains(weaponName);
    }
    public void ChangeStartWeapon(WeaponData weaponData)
    {
        m_CurrentStartWeapon = weaponData;
        SaveCurrentStartWeapon();
    }
    public void AddCurrentUnlockWeaponProcess(float value)
    {
        m_CurrentUnlockWeaponProgress += value;
        if (m_CurrentUnlockWeaponProgress >= 1)
        {
            m_CurrentUnlockWeaponProgress -= 1;
            UnlockWeapon(m_CurrentUnlockWeapon.weaponName);
            m_CurrentUnlockWeapon = WeaponManager.Instance.GetRandomLockWeapon();
        }
        SaveCurrentUnlockWeapon();
    }
    #endregion
}
