using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.AI;

public class PlayerInputManager : Singleton<PlayerInputManager>
{
    public Transform m_StandHere;
    public bool m_CanMoveGroup;
    [Header("X Movement")]
    public float m_MovementSpeedX;
    public float m_Threshold;

    [Header("Clamp Position")]
    public float m_MinValue;
    public float m_MaxValue;

    private float m_LastPositionX;
    private float m_CurrentPositionX;
    public Vector2 m_MousePosition;
    private float m_Delta;
    private int m_MoveDirectionX;
    public float m_MaxMousePositionX;

    private void Awake()
    {
        m_CanMoveGroup = false;
        m_MaxMousePositionX = Screen.width;
    }
    private void Start() {
        
    }

    private void Update()
    {
        GetInput();
        MoveGroup();
    }

    private void GetInput()
    {
        if (!m_CanMoveGroup)
        {
            m_MoveDirectionX = 0;
            return;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            m_MousePosition = Input.mousePosition;
            m_CurrentPositionX = m_MousePosition.x;
            m_LastPositionX = m_MousePosition.x;
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            m_MousePosition = Input.mousePosition;
            m_CurrentPositionX = m_MousePosition.x;
            m_Delta = Mathf.Abs(m_CurrentPositionX - m_LastPositionX);
            if (m_Delta > m_Threshold)
            {
                m_MoveDirectionX = m_CurrentPositionX > m_LastPositionX ? 1 : -1;
                m_LastPositionX = m_MousePosition.x;
            }
            else
            {
                m_MoveDirectionX = 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            m_MoveDirectionX = 0;
        }
    }

    private void MoveGroup()
    {
        Vector3 standHerePos = m_StandHere.localPosition;
        float xPosition = standHerePos.x + m_MovementSpeedX * m_MoveDirectionX * Time.deltaTime * m_Delta * 452 / m_MaxMousePositionX;
        m_StandHere.localPosition = new Vector3(Mathf.Clamp(xPosition, -5 - m_MinValue, 5 - m_MaxValue),0,0);
    }
    public void UpdateClampValue(PlayerCharacter character)
    {
        float xPosition = character.m_CurrentPosition.transform.localPosition.x;
        m_MinValue = xPosition < m_MinValue ? xPosition : m_MinValue;
        m_MaxValue = xPosition > m_MaxValue ? xPosition : m_MaxValue;
    }
    public void ClearInput()
    {
        m_LastPositionX = 0;
        m_CurrentPositionX = 0;
        m_MoveDirectionX = 0;
        m_MinValue = 0;
        m_MaxValue = 0;
    }

}
