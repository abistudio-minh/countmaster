using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : Singleton<PrefabManager>
{
    public GameObject m_PlayerCharacter;
    public List<GameObject> m_EnemyList;
    public List<GameObject> m_BulletList;
    private void Awake()
    {
        InitPrefab();
    }
    public void InitPrefab()
    {
        CreatEnemyPool();
        CreatePlayerPool();
        CreatBulletPool();
    }
    public void CreatEnemyPool()
    {
        for (int i = 0; i < m_EnemyList.Count; i++)
        {
            SimplePool.Preload(m_EnemyList[i], 20);
        }
    }
    public void CreatePlayerPool()
    {
        SimplePool.Preload(m_PlayerCharacter, 108);
    }
    public void CreatBulletPool()
    {
        for (int i = 0; i < m_BulletList.Count; i++)
        {
            SimplePool.Preload(m_BulletList[i], 50);
        }
    }
}
