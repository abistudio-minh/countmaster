using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : Singleton<WeaponManager>
{
    public List<WeaponData> m_WeaponList = new List<WeaponData>();
    public WeaponData GetWeaponData(string weaponName)
    {
        for (int i = 0; i < m_WeaponList.Count; i++)
        {
            if (m_WeaponList[i].weaponName == weaponName)
            {
                return m_WeaponList[i];
            }
        }
        return null;
    }
    public WeaponData GetRandomLockWeapon()
    {
        if (PlayerData.Instance.m_UnlockWeapon.Count == m_WeaponList.Count) return null;
        List<WeaponData> lockWeapons = new List<WeaponData>();
        for (int i = 0; i < m_WeaponList.Count; i++)
        {
            if (!PlayerData.Instance.m_UnlockWeapon.Contains(m_WeaponList[i].weaponName))
            {
                lockWeapons.Add(m_WeaponList[i]);
            }
        }
        return lockWeapons[Random.Range(0, lockWeapons.Count)];

    }
}
