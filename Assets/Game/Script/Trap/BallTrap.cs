using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrap : MonoBehaviour,IActivate
{   
    bool isActivate = false;
    float speed = 20f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!isActivate) return;
        transform.position += transform.forward * Time.deltaTime * speed; 
    }

    public void SetActive(){
        isActivate = true;
        Destroy();
    }

    void Destroy(){
        Destroy(gameObject,5f);
    }
}
