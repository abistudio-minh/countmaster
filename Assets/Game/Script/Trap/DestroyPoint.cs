using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPoint : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GroupManager.Instance.RemoveSinglePlayerCharacter(other.GetComponent<PlayerCharacter>());
    }
}
