using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleTrap : MonoBehaviour,IActivate
{   
    bool startClosing = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!startClosing)   return;
        CloseHole();
    }

    public void SetActive(){
        transform.position = new Vector3(transform.position.x,0.2f,transform.position.z);
        Invoke("CloseHole",2f);
    }

    void CloseHole(){
        startClosing = true;
        float zScale = 5f * Time.deltaTime;
        //transform.localScale = new Vector3(transform.localScale.x,transform.localScale.y,zScale);
        transform.localScale -= new Vector3(0,0,zScale);
        if(transform.localScale.z < 0){
            Destroy(gameObject);
        }   
    }
}
