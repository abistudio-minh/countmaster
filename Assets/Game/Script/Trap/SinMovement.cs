using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinMovement : MonoBehaviour
{
    private Vector3 _startPosition;
 
    void Start () 
    {
        _startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
      transform.position = _startPosition + transform.right * Mathf.Sin(Time.time*1.5f) * 4f ;

    }
}
