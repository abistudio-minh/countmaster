using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTrap : MonoBehaviour
{   
    public Transform Trap;
    private bool isTrigger = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(!isTrigger){
            isTrigger = true;
            Trap.GetComponent<IActivate>().SetActive();
        }
    }
}
