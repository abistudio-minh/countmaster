using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BannerCountCharacter : MonoBehaviour
{
    public Text m_CharacterCount;
    private void Awake()
    {
        EventManager.StartListening("ChangeCharacterCount", UpdateCharacterCount);
    }
    private void OnEnable()
    {
        UpdateCharacterCount();
    }
    private void Update()
    {
        Vector3 pos = CameraManager.Instance.m_MainCamera.WorldToScreenPoint(GroupManager.Instance.m_StandHere.position);
        transform.position = pos;
    }
    public void UpdateCharacterCount()
    {
        m_CharacterCount.text = GroupManager.Instance.m_CharacterCount.ToString();
    }
}
