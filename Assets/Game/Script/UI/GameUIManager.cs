using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class GameUIManager : Singleton<GameUIManager>
{
    [Header("Panel")]
    public PanelMainMenu m_PanelMainMenu;
    public PanelEndGame m_PanelEndGame;
    public PanelShop m_PanelShop;

    [Header("Ingame Object")]
    public BannerCountCharacter m_BannerCharacterCount;

    public void ScaleByTime(GameObject owner, float startValue, float endValue, float time)
    {
        owner.transform.transform.localScale = Vector3.one * startValue;
        owner.transform.DOScale(Vector3.one * endValue, time);
    }
    public void ScaleByTime(GameObject owner, float time)
    {
        Vector3 currentScale = owner.transform.localScale;
        owner.transform.transform.localScale = Vector3.zero;
        owner.transform.DOScale(currentScale, time);
    }
}
