using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Light : MonoBehaviour
{
    private void Start()
    {
        transform.DORotate(Vector3.forward * 180, 2).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
    }
}
