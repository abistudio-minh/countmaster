using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Canvas), typeof(GraphicRaycaster))]
public class Panel : MonoBehaviour
{
    public void Awake()
    {
        Init();
    }
    private void OnEnable()
    {
       
    }
    public virtual void Init()
    {

    }
    public virtual void OnActive()
    {
        gameObject.SetActive(true);


    }
    public virtual void OnDeactive()
    {
        gameObject.SetActive(false);
    }
}
