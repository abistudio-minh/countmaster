using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
public class PanelCoin : MonoBehaviour
{
    public TextMeshProUGUI m_CoinText;
    public int m_CurrentCoin;
    public int m_Coin;

    private Tween m_CoinTween;
    private void Awake()
    {
        EventManager.StartListening("CoinChange", OnCoinChange);
    }
    private void Start()
    {
        m_CurrentCoin = PlayerData.Instance.m_CurrentCoin;
        m_Coin = m_CurrentCoin;
        ChangeCoinText();
    }
    public void OnCoinChange()
    {
        m_CoinTween?.Kill();
        m_Coin = PlayerData.Instance.m_CurrentCoin;
        m_CoinTween = DOTween.To(() => m_CurrentCoin, x => m_CurrentCoin = x, m_Coin, 1).SetEase(Ease.Linear).OnUpdate(ChangeCoinText);
    }
    public void ChangeCoinText()
    {
        m_CoinText.text = m_CurrentCoin.ToString();
    }

}
