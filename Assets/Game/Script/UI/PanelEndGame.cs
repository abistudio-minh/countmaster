using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class PanelEndGame : Panel
{
    public TextMeshProUGUI m_TempText;

    public bool m_IsWin;
    public WeaponData m_CurrentUnlockWeaponData;
    public float m_Bonus;
    public int m_Coin;
    public float m_CurrentUnlockWeaponProcess;
    public bool m_Wait;

    public Transform m_Arrow;
    public TextMeshProUGUI m_BonusText;
    public TextMeshProUGUI m_CoinBonusText;
    public TextMeshProUGUI m_CoinText;
    public TextMeshProUGUI m_ProcessText;

    public GameObject m_Success;
    public GameObject m_Failure;
    public GameObject m_NewWeapon;
    public Image m_CurrentUnlockWeaponBG;
    public Image m_CurrentUnlockWeapon;
    public Image m_UnlockWeapon;
    public GameObject m_BonusSlide;
    public Button m_ClaimBonusButton;
    public Button m_ClaimButton;
    public Button m_GetButton;
    public Button m_BackButton;

    public List<GameObject> m_BlockButtonImage;
    private Tween m_SlideTween;
    private Coroutine m_Coroutine;

    public override void Init()
    {
        base.Init();
        m_Wait = false;
        GameManager.Instance.AddClickEvent(m_ClaimButton, OnClickClaimButton);
        GameManager.Instance.AddClickEvent(m_ClaimBonusButton, OnClickClaimBonusButton);
        GameManager.Instance.AddClickEvent(m_GetButton, OnClickGetButton);
        GameManager.Instance.AddClickEvent(m_BackButton, OnClickBackButton);
    }
    public override void OnActive()
    {
        base.OnActive();
        
        StartCoroutine(CoActive());
    }
    private void Update()
    {
        UpdateBonusCoinText();
    }
    public void OnClickClaimButton()
    {
        m_SlideTween?.Kill();
        PlayerData.Instance.AddCoin(m_Coin);
        GameManager.Instance.DelayAction(GameManager.Instance.OnRestartGame, 2);
        SetInteractableClaimButton(false);
    }
    public void OnClickClaimBonusButton()
    {
        RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.BONUS_COIN, ClaimBonus);
    }
    public void ClaimBonus()
    {
        m_SlideTween?.Kill();
        PlayerData.Instance.AddCoin(Mathf.RoundToInt(m_Coin * m_Bonus));
        GameManager.Instance.DelayAction(GameManager.Instance.OnRestartGame, 2);
        SetInteractableClaimButton(false);
    }
    public void OnClickGetButton()
    {
        RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.OWN_WEAPON, OwnWeapon);
    }
    public void OwnWeapon()
    {
        PlayerData.Instance.OwnWeapon(PlayerData.Instance.m_CurrentUnlockWeapon.weaponName);
        m_NewWeapon.SetActive(false);
        m_Wait = false;
    }
    public void OnClickBackButton()
    {
        m_NewWeapon.SetActive(false);
        m_Wait = false;
    }
    public void Setup(bool isWin)
    {
        m_TempText.text = string.Format("{0}. Enemy kill: {1}", isWin ? "Win game" : "Lose game", EnemyManager.Instance.enemyKill);
        m_IsWin = isWin;
        m_Success.SetActive(m_IsWin);
        m_Failure.SetActive(!m_IsWin);

        if (m_IsWin)
        {
            m_CurrentUnlockWeaponData = PlayerData.Instance.m_CurrentUnlockWeapon;
            m_CurrentUnlockWeaponProcess = PlayerData.Instance.m_CurrentUnlockWeaponProgress;
            if (m_CurrentUnlockWeaponData != null)
            {
                m_CurrentUnlockWeaponBG.sprite = m_CurrentUnlockWeaponData.weaponImage;
                m_CurrentUnlockWeapon.sprite = m_CurrentUnlockWeaponData.weaponImage;
                m_UnlockWeapon.sprite = m_CurrentUnlockWeaponData.weaponImage;
            }
            UpdateProcess();
        }
        // TODO: +++++ Endgame add coin
        m_Coin = (EnemyManager.Instance.enemyKill + 1) * (10 + PlayerData.Instance.m_CurrentIncomeUpgrade);
        m_CoinText.text = m_Coin.ToString();

        m_CurrentUnlockWeaponBG.gameObject.SetActive(false);

        m_BonusSlide.SetActive(false);
        m_ClaimBonusButton.gameObject.SetActive(false);
        m_ClaimButton.gameObject.SetActive(false);

        SetInteractableClaimButton(true);

    }
    public void StartBonnusSlide()
    {
        m_Arrow.localPosition = new Vector3(-330, -50, 0);
        m_SlideTween = m_Arrow.DOLocalMoveX(330, 1).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }
    public void UpdateBonusCoinText()
    {
        switch ((int)(Mathf.Abs(m_Arrow.localPosition.x) + 55) / 110)
        {
            case 0:
                m_Bonus = 3;
                break;
            case 1:
                m_Bonus = 2.5f;
                break;
            case 2:
                m_Bonus = 2f;
                break;
            default:
                m_Bonus = 1.5f;
                break;
        }
        m_BonusText.text = string.Format("X{0}", m_Bonus);
        m_CoinBonusText.text = Mathf.RoundToInt(m_Coin * m_Bonus).ToString();
    }
    public void SetInteractableClaimButton(bool value)
    {
        for (int i = 0; i < m_BlockButtonImage.Count; i++)
        {
            m_BlockButtonImage[i].SetActive(!value);
        }
        m_ClaimBonusButton.interactable = value;
        m_ClaimButton.interactable = value;
    }
    IEnumerator CoActive()
    {
        float process = 1f;
        GameUIManager.Instance.ScaleByTime(gameObject, 0.5f);
        yield return new WaitForSeconds(0.5f);
        if (m_IsWin && m_CurrentUnlockWeaponData != null)
        {
            float newProcess = m_CurrentUnlockWeaponProcess + process;

            m_CurrentUnlockWeaponBG.gameObject.SetActive(true);
            GameUIManager.Instance.ScaleByTime(m_CurrentUnlockWeaponBG.gameObject, 0.5f);
            yield return new WaitForSeconds(1f);
            DOTween.To(() => m_CurrentUnlockWeaponProcess, x => m_CurrentUnlockWeaponProcess = x, newProcess, 1).SetEase(Ease.Linear).OnUpdate(UpdateProcess);
            yield return new WaitForSeconds(2f);
            if (newProcess >= 1)
            {
                m_Wait = true;
                m_NewWeapon.SetActive(true);
                GameUIManager.Instance.ScaleByTime(m_NewWeapon, 0.5f);
            }
            yield return new WaitWhile(() => m_Wait);
            yield return new WaitForSeconds(0.5f);
            PlayerData.Instance.AddCurrentUnlockWeaponProcess(process);
        }

        m_BonusSlide.SetActive(true);
        m_ClaimBonusButton.gameObject.SetActive(true);
        GameUIManager.Instance.ScaleByTime(m_BonusSlide, 0.5f);
        GameUIManager.Instance.ScaleByTime(m_ClaimBonusButton.gameObject, 0.5f);
        StartBonnusSlide();
        yield return new WaitForSeconds(3);

        m_ClaimButton.gameObject.SetActive(true);
        GameUIManager.Instance.ScaleByTime(m_ClaimButton.gameObject, 0.5f);

    }
    public void UpdateProcess()
    {
        m_CurrentUnlockWeapon.fillAmount = m_CurrentUnlockWeaponProcess;
        m_ProcessText.text = string.Format("{0}%", Mathf.RoundToInt(m_CurrentUnlockWeaponProcess * 100));
    }

}
