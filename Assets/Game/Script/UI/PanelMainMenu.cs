using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelMainMenu : Panel
{
    public Button m_StartGameButton;
    public Button m_ShopButton;
    public UpgradeButton m_ManPowerUpgradeButton;
    public UpgradeButton m_IncomeUpgradeButton;
    public UpgradeButton m_DamageUpgradeButton;

    public int m_ManPowerUpgradeLevel;
    public int m_IncomeUpgradeLevel;
    public int m_DamageUpgradeLevel;

    public override void Init()
    {
        base.Init();
        GameManager.Instance.AddClickEvent(m_StartGameButton, OnClickStartGameButton);
        GameManager.Instance.AddClickEvent(m_ShopButton, OnClickShopButton);
        SetupButton();
    }
    public override void OnActive()
    {
        base.OnActive();
        SetupButton();
    }
    public void OnClickStartGameButton()
    {
        GameManager.Instance.OnStartGame();
    }
    public void OnClickShopButton()
    {
        GameUIManager.Instance.m_PanelShop.OnActive();
        OnDeactive();
    }
    public void OnClickManPowerUpgradeButton()
    {
        if (m_ManPowerUpgradeButton.m_CurrentCost <= PlayerData.Instance.m_CurrentCoin)
        {
            PlayerData.Instance.ConsumeCoin(m_ManPowerUpgradeButton.m_CurrentCost);
            ManPowerUpgrade();
        }
        else
        {
            RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.UPGRADE_PLAYER, ManPowerUpgrade);
        }
    }
    public void ManPowerUpgrade()
    {
        PlayerData.Instance.m_CurrentManPowerUpgrade++;
        PlayerData.Instance.SaveCurrentManPowerUpgrade();
        GroupManager.Instance.AddSinglePlayerCharacter(false);
        SetupButton();
    }
    public void OnClickIncomeUpgradeButton()
    {
        if (m_IncomeUpgradeButton.m_CurrentCost <= PlayerData.Instance.m_CurrentCoin)
        {
            PlayerData.Instance.ConsumeCoin(m_IncomeUpgradeButton.m_CurrentCost);
            IncomeUpgrade();
        }
        else
        {
            RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.UPGRADE_PLAYER, IncomeUpgrade);
        }

    }
    public void IncomeUpgrade()
    {
        PlayerData.Instance.m_CurrentIncomeUpgrade++;
        PlayerData.Instance.SaveCurrentIncomeUpgrade();

        SetupButton();
    }
    public void OnClickDamageUpgradeButton()
    {
        if (m_DamageUpgradeButton.m_CurrentCost <= PlayerData.Instance.m_CurrentCoin)
        {
            PlayerData.Instance.ConsumeCoin(m_DamageUpgradeButton.m_CurrentCost);
            DamageUpgrade();
        }
        else
        {
            RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.UPGRADE_PLAYER, DamageUpgrade);
        }

    }
    public void DamageUpgrade()
    {
        PlayerData.Instance.m_CurrentDamageUpgrade++;
        PlayerData.Instance.SaveCurrentDamageUpgrade();

        SetupButton();
    }
    public void SetupButton()
    {
        // TODO: +++++ Change cost
        m_ManPowerUpgradeLevel = PlayerData.Instance.m_CurrentManPowerUpgrade;
        m_ManPowerUpgradeButton.Setup(m_ManPowerUpgradeLevel, m_ManPowerUpgradeLevel * 300);

        m_IncomeUpgradeLevel = PlayerData.Instance.m_CurrentIncomeUpgrade;
        m_IncomeUpgradeButton.Setup(m_IncomeUpgradeLevel, m_IncomeUpgradeLevel * 300);

        m_DamageUpgradeLevel = PlayerData.Instance.m_CurrentDamageUpgrade;
        m_DamageUpgradeButton.Setup(m_DamageUpgradeLevel, m_DamageUpgradeLevel * 300);
    }

}
