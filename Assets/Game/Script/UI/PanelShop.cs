using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelShop : Panel
{
    public Button m_ExitButton;
    public Transform m_Content;
    public ShopButton m_ShopButtonPrefab;
    public List<ShopButton> m_ShopButtonList;

    public override void Init()
    {
        base.Init();
        GameManager.Instance.AddClickEvent(m_ExitButton, OnClickExitButton);
        EventManager.StartListening("UpdateShopButton", UpdateShopButton);
        InitShopButton();
    }
    public override void OnActive()
    {
        base.OnActive();
        UpdateShopButton();
    }
    public void OnClickExitButton()
    {
        GameUIManager.Instance.m_PanelMainMenu.OnActive();
        OnDeactive();
    }
    public void InitShopButton()
    {
        List<WeaponData> weaponList = WeaponManager.Instance.m_WeaponList;
        for (int i = 0; i < weaponList.Count; i++)
        {
            ShopButton tempButton = Instantiate(m_ShopButtonPrefab, m_Content);
            tempButton.Setup(weaponList[i]);
            m_ShopButtonList.Add(tempButton);
        }
    }
    public void UpdateShopButton()
    {
        for (int i = 0; i < m_ShopButtonList.Count; i++)
        {
            m_ShopButtonList[i].UpdateButton();
        }
    }
}
