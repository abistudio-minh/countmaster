using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class ShopButton : MonoBehaviour
{
    public WeaponData m_WeaponData;
    public Button m_Button;
    public GameObject m_Get;
    public GameObject m_Lock;
    public GameObject m_InUse;
    public Image m_WeaponImage;

    public bool m_IsUnlock;
    public bool m_IsOwn;
    private void Awake()
    {
        GameManager.Instance.AddClickEvent(m_Button, OnClickButton);
    }
    public void Setup(WeaponData weaponData)
    {
        m_WeaponData = weaponData;
        m_WeaponImage.sprite = weaponData.weaponImage;
    }
    public void UpdateButton()
    {
        m_IsUnlock = PlayerData.Instance.IsUnlockWeapon(m_WeaponData.weaponName);
        m_IsOwn = PlayerData.Instance.IsOwnWeapon(m_WeaponData.weaponName);

        m_InUse.SetActive(PlayerData.Instance.m_CurrentStartWeapon == m_WeaponData);
        m_Get.SetActive(m_IsUnlock && !m_IsOwn);
        m_Lock.SetActive(!m_IsUnlock && !m_IsOwn);
        m_Button.gameObject.SetActive(m_IsUnlock);
        m_WeaponImage.color = m_IsUnlock ? Color.white : Color.black;
    }
    public void OnClickButton()
    {
        if (m_IsOwn)
        {
            PlayerData.Instance.ChangeStartWeapon(m_WeaponData);
        }
        else
        {
            RewardAdsManager.Instance.ShowRewardAds(WatchVideoRewardType.OWN_WEAPON, OwnWeapon);
        }
        EventManager.TriggerEvent("UpdateShopButton");
    }
    public void OwnWeapon()
    {
        PlayerData.Instance.OwnWeapon(m_WeaponData.weaponName);
    }
}
