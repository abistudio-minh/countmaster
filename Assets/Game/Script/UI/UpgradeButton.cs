using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UpgradeButton : MonoBehaviour
{
    public int m_CurrentLevel;
    public int m_CurrentCost;

    public TextMeshProUGUI m_LevelText;
    public TextMeshProUGUI m_CostText;

    public GameObject m_Cost;
    public GameObject m_Watch;
    public void Setup(int currentLevel, int currentCost)
    {
        m_CurrentLevel = currentLevel;
        m_CurrentCost = currentCost;

        m_LevelText.text = "Lv" + m_CurrentLevel.ToString();
        m_CostText.text = m_CurrentCost.ToString();

        if (PlayerData.Instance.m_CurrentCoin < m_CurrentCost)
        {
            m_Cost.SetActive(false);
            m_Watch.SetActive(true);
        }
        else
        {
            m_Cost.SetActive(true);
            m_Watch.SetActive(false);
        }
    }
}
