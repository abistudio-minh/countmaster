using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private Transform m_Transform;
    public Transform Transform
    {
        get
        {
            if (m_Transform == null)
            {
                m_Transform = transform;
            }
            return m_Transform;
        }
    }
    protected int m_AttackDame;
    protected float m_BulletMovementSpeed;
    [SerializeField] protected GameObject m_Bullet;
    [SerializeField] protected Transform m_AttackPosition;

    public void Init(int attackDame, float bulletMovementSpeed, GameObject bullet)
    {
        m_AttackDame = attackDame;
        m_BulletMovementSpeed = bulletMovementSpeed;
        m_Bullet = bullet;
    }
    private void Update()
    {
        FixRotate();
    }
    public virtual void Attack()
    {


    }
    public virtual void FixRotate()
    {

    }
}
