using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 1)]
public class WeaponData : ScriptableObject
{
    [Header("Weapon")]
    public GameObject weaponObject;
    public string weaponName;
    public string weaponAnimationString;
    public Sprite weaponImage;

    [Header("Bullet")]
    public GameObject weaponBullet;
    public int weaponBulletDamage;
    public float weaponBulletMovementSpeed;
}
