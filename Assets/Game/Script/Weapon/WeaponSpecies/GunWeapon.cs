using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunWeapon : Weapon
{
    public override void Attack()
    {
        base.Attack();
        Projectile temp = SimplePool.Spawn(m_Bullet, m_AttackPosition.position, GroupManager.Instance.m_Group.rotation).GetComponent<Projectile>();
        temp.Init(m_AttackDame, m_BulletMovementSpeed);
    }
    public override void FixRotate()
    {
        base.FixRotate();
        Transform.rotation = GroupManager.Instance.m_Group.rotation;
    }
}
